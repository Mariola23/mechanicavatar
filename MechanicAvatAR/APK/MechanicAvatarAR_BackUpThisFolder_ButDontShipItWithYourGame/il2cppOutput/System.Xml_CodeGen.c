﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void System.Xml.XmlReader::.cctor()
extern void XmlReader__cctor_m128907AB1DC1AD053C3CDF366B6A9C5F9AE84D83 (void);
// 0x00000002 System.Void System.Xml.Serialization.XmlIgnoreAttribute::.ctor()
extern void XmlIgnoreAttribute__ctor_m864B47EB1DF49D8D8324BD2A6D67381A9DC8DA80 (void);
static Il2CppMethodPointer s_methodPointers[2] = 
{
	XmlReader__cctor_m128907AB1DC1AD053C3CDF366B6A9C5F9AE84D83,
	XmlIgnoreAttribute__ctor_m864B47EB1DF49D8D8324BD2A6D67381A9DC8DA80,
};
static const int32_t s_InvokerIndices[2] = 
{
	16632,
	10250,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Xml_CodeGenModule;
const Il2CppCodeGenModule g_System_Xml_CodeGenModule = 
{
	"System.Xml.dll",
	2,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;

struct unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902;
struct unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2;
struct unitytls_tlsctx_tF8BBCBFE1E957B846442DED65ECB89BC5307DEAE;
struct unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17;
struct unitytls_x509name_t8A1108C917795D8FE946B50769ACE51489C7BF5D;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// Realms.Native.TableKey
struct TableKey_tDE0311CA4D5C298F8BB7E0352BAEE9910E60B713 
{
	// System.UInt32 Realms.Native.TableKey::Value
	uint32_t ___Value_0;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// Realms.Native.PrimitiveValue/<decimal_bits>e__FixedBuffer
struct U3Cdecimal_bitsU3Ee__FixedBuffer_tF7D50EE072C933FF8FA41523147EAE0AF67B528D 
{
	union
	{
		struct
		{
			// System.UInt64 Realms.Native.PrimitiveValue/<decimal_bits>e__FixedBuffer::FixedElementField
			uint64_t ___FixedElementField_0;
		};
		uint8_t U3Cdecimal_bitsU3Ee__FixedBuffer_tF7D50EE072C933FF8FA41523147EAE0AF67B528D__padding[16];
	};
};

// Realms.Native.PrimitiveValue/<guid_bytes>e__FixedBuffer
struct U3Cguid_bytesU3Ee__FixedBuffer_tC99494C5FB5D0B6DDFDBECE6BB8FEFAEE7165DC3 
{
	union
	{
		struct
		{
			// System.Byte Realms.Native.PrimitiveValue/<guid_bytes>e__FixedBuffer::FixedElementField
			uint8_t ___FixedElementField_0;
		};
		uint8_t U3Cguid_bytesU3Ee__FixedBuffer_tC99494C5FB5D0B6DDFDBECE6BB8FEFAEE7165DC3__padding[16];
	};
};

// Realms.Native.PrimitiveValue/<object_id_bytes>e__FixedBuffer
struct U3Cobject_id_bytesU3Ee__FixedBuffer_t8744739B792C1B30310C858286163338499BA3FB 
{
	union
	{
		struct
		{
			// System.Byte Realms.Native.PrimitiveValue/<object_id_bytes>e__FixedBuffer::FixedElementField
			uint8_t ___FixedElementField_0;
		};
		uint8_t U3Cobject_id_bytesU3Ee__FixedBuffer_t8744739B792C1B30310C858286163338499BA3FB__padding[12];
	};
};

// Realms.Native.PrimitiveValue/TimestampValue
struct TimestampValue_t6D61E9CC604EF7895FE1872840E8227341DD69DC 
{
	// System.Int64 Realms.Native.PrimitiveValue/TimestampValue::seconds
	int64_t ___seconds_3;
	// System.Int32 Realms.Native.PrimitiveValue/TimestampValue::nanoseconds
	int32_t ___nanoseconds_4;
};

// Mono.Unity.UnityTls/unitytls_x509_ref
struct unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 
{
	// System.UInt64 Mono.Unity.UnityTls/unitytls_x509_ref::handle
	uint64_t ___handle_0;
};

// Mono.Unity.UnityTls/unitytls_x509list_ref
struct unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 
{
	// System.UInt64 Mono.Unity.UnityTls/unitytls_x509list_ref::handle
	uint64_t ___handle_0;
};

// Realms.MarshaledVector`1<Realms.Sync.Native.StringStringPair>
struct MarshaledVector_1_t69FBB95AB4D109397E04B8ADECF730DDDA5F13B2 
{
	// System.IntPtr Realms.MarshaledVector`1::Items
	intptr_t ___Items_0;
	// System.IntPtr Realms.MarshaledVector`1::Count
	intptr_t ___Count_1;
};

// Realms.MarshaledVector`1<Realms.Native.SyncError/CompensatingWriteInfo>
struct MarshaledVector_1_tF5B4FE4DF9EA05670ED946FCA359399F2E5A1049 
{
	// System.IntPtr Realms.MarshaledVector`1::Items
	intptr_t ___Items_0;
	// System.IntPtr Realms.MarshaledVector`1::Count
	intptr_t ___Count_1;
};

// Realms.NativeException
struct NativeException_tD0D4AE8BF0E1593EA969F3725B458DDA9B8D25A7 
{
	// Realms.Exceptions.RealmExceptionCodes Realms.NativeException::code
	int32_t ___code_0;
	// Realms.Exceptions.RealmExceptionCategories Realms.NativeException::categories
	int32_t ___categories_1;
	// System.Byte* Realms.NativeException::messageBytes
	uint8_t* ___messageBytes_2;
	// System.IntPtr Realms.NativeException::messageLength
	intptr_t ___messageLength_3;
	// System.IntPtr Realms.NativeException::managedException
	intptr_t ___managedException_4;
};

// Realms.Native.Schema
struct Schema_t119F81EC190B6E7B0C4776F0D55C0DC834A2E840 
{
	// System.IntPtr Realms.Native.Schema::objects
	intptr_t ___objects_0;
	// System.Int32 Realms.Native.Schema::objects_len
	int32_t ___objects_len_1;
	// System.IntPtr Realms.Native.Schema::properties
	intptr_t ___properties_2;
};

// Realms.Native.StringValue
struct StringValue_tA17D7236E5C9F0E6D7A77F144E825D848CA568B8 
{
	// System.Byte* Realms.Native.StringValue::data
	uint8_t* ___data_0;
	// System.IntPtr Realms.Native.StringValue::size
	intptr_t ___size_1;
};

// Realms.Native.PrimitiveValue/BinaryValue
struct BinaryValue_t3E6C3999D1D5656B4E0F1A33202BE7832DD64A5D 
{
	// System.Byte* Realms.Native.PrimitiveValue/BinaryValue::data
	uint8_t* ___data_0;
	// System.IntPtr Realms.Native.PrimitiveValue/BinaryValue::size
	intptr_t ___size_1;
};

// Realms.Native.PrimitiveValue/LinkValue
struct LinkValue_t2E971397736EAD9583513BF15C18315B51B81EA3 
{
	// System.IntPtr Realms.Native.PrimitiveValue/LinkValue::object_ptr
	intptr_t ___object_ptr_0;
	// Realms.Native.TableKey Realms.Native.PrimitiveValue/LinkValue::table_key
	TableKey_tDE0311CA4D5C298F8BB7E0352BAEE9910E60B713 ___table_key_1;
};

// Realms.Native.PrimitiveValue
struct PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int64 Realms.Native.PrimitiveValue::int_value
			int64_t ___int_value_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___int_value_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single Realms.Native.PrimitiveValue::float_value
			float ___float_value_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___float_value_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double Realms.Native.PrimitiveValue::double_value
			double ___double_value_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___double_value_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Realms.Native.PrimitiveValue/<decimal_bits>e__FixedBuffer Realms.Native.PrimitiveValue::decimal_bits
			U3Cdecimal_bitsU3Ee__FixedBuffer_tF7D50EE072C933FF8FA41523147EAE0AF67B528D ___decimal_bits_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			U3Cdecimal_bitsU3Ee__FixedBuffer_tF7D50EE072C933FF8FA41523147EAE0AF67B528D ___decimal_bits_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Realms.Native.PrimitiveValue/<object_id_bytes>e__FixedBuffer Realms.Native.PrimitiveValue::object_id_bytes
			U3Cobject_id_bytesU3Ee__FixedBuffer_t8744739B792C1B30310C858286163338499BA3FB ___object_id_bytes_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			U3Cobject_id_bytesU3Ee__FixedBuffer_t8744739B792C1B30310C858286163338499BA3FB ___object_id_bytes_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Realms.Native.PrimitiveValue/<guid_bytes>e__FixedBuffer Realms.Native.PrimitiveValue::guid_bytes
			U3Cguid_bytesU3Ee__FixedBuffer_tC99494C5FB5D0B6DDFDBECE6BB8FEFAEE7165DC3 ___guid_bytes_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			U3Cguid_bytesU3Ee__FixedBuffer_tC99494C5FB5D0B6DDFDBECE6BB8FEFAEE7165DC3 ___guid_bytes_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___dontuse_6_OffsetPadding[8];
			// System.Int64 Realms.Native.PrimitiveValue::dontuse
			int64_t ___dontuse_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___dontuse_6_OffsetPadding_forAlignmentOnly[8];
			int64_t ___dontuse_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Realms.Native.StringValue Realms.Native.PrimitiveValue::string_value
			StringValue_tA17D7236E5C9F0E6D7A77F144E825D848CA568B8 ___string_value_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			StringValue_tA17D7236E5C9F0E6D7A77F144E825D848CA568B8 ___string_value_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Realms.Native.PrimitiveValue/BinaryValue Realms.Native.PrimitiveValue::data_value
			BinaryValue_t3E6C3999D1D5656B4E0F1A33202BE7832DD64A5D ___data_value_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			BinaryValue_t3E6C3999D1D5656B4E0F1A33202BE7832DD64A5D ___data_value_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Realms.Native.PrimitiveValue/TimestampValue Realms.Native.PrimitiveValue::timestamp_value
			TimestampValue_t6D61E9CC604EF7895FE1872840E8227341DD69DC ___timestamp_value_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			TimestampValue_t6D61E9CC604EF7895FE1872840E8227341DD69DC ___timestamp_value_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// Realms.Native.PrimitiveValue/LinkValue Realms.Native.PrimitiveValue::link_value
			LinkValue_t2E971397736EAD9583513BF15C18315B51B81EA3 ___link_value_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			LinkValue_t2E971397736EAD9583513BF15C18315B51B81EA3 ___link_value_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Type_11_OffsetPadding[16];
			// Realms.RealmValueType Realms.Native.PrimitiveValue::Type
			uint8_t ___Type_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Type_11_OffsetPadding_forAlignmentOnly[16];
			uint8_t ___Type_11_forAlignmentOnly;
		};
	};
};

// Realms.Native.SyncError
struct SyncError_tE2E8948799F75F68D3627FD39942895E146334E3 
{
	// Realms.Sync.Exceptions.ErrorCode Realms.Native.SyncError::error_code
	int32_t ___error_code_0;
	// Realms.Native.StringValue Realms.Native.SyncError::message
	StringValue_tA17D7236E5C9F0E6D7A77F144E825D848CA568B8 ___message_1;
	// Realms.Native.StringValue Realms.Native.SyncError::log_url
	StringValue_tA17D7236E5C9F0E6D7A77F144E825D848CA568B8 ___log_url_2;
	// System.Boolean Realms.Native.SyncError::is_client_reset
	bool ___is_client_reset_3;
	// Realms.MarshaledVector`1<Realms.Sync.Native.StringStringPair> Realms.Native.SyncError::user_info_pairs
	MarshaledVector_1_t69FBB95AB4D109397E04B8ADECF730DDDA5F13B2 ___user_info_pairs_4;
	// Realms.MarshaledVector`1<Realms.Native.SyncError/CompensatingWriteInfo> Realms.Native.SyncError::compensating_writes
	MarshaledVector_1_tF5B4FE4DF9EA05670ED946FCA359399F2E5A1049 ___compensating_writes_5;
};

// Realms.Sync.Native.AppError
struct AppError_t76E59E2102C582D663A3D26011D7CA63B7759946 
{
	// System.Boolean Realms.Sync.Native.AppError::is_null
	bool ___is_null_0;
	// Realms.Native.PrimitiveValue Realms.Sync.Native.AppError::message
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___message_1;
	// Realms.Native.PrimitiveValue Realms.Sync.Native.AppError::error_category
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___error_category_2;
	// Realms.Native.PrimitiveValue Realms.Sync.Native.AppError::logs_link
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___logs_link_3;
	// System.Int32 Realms.Sync.Native.AppError::http_status_code
	int32_t ___http_status_code_4;
};

// Realms.Sync.Native.Subscription
struct Subscription_t71D228FFF062330388899D9060576B43B033D18E 
{
	// Realms.Native.PrimitiveValue Realms.Sync.Native.Subscription::id
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___id_0;
	// Realms.Native.PrimitiveValue Realms.Sync.Native.Subscription::name
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___name_1;
	// Realms.Native.PrimitiveValue Realms.Sync.Native.Subscription::object_type
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___object_type_2;
	// Realms.Native.PrimitiveValue Realms.Sync.Native.Subscription::query
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___query_3;
	// Realms.Native.PrimitiveValue Realms.Sync.Native.Subscription::created_at
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___created_at_4;
	// Realms.Native.PrimitiveValue Realms.Sync.Native.Subscription::updated_at
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___updated_at_5;
	// System.Boolean Realms.Sync.Native.Subscription::has_value
	bool ___has_value_6;
};

// Realms.Native.HttpClientTransport/HttpClientRequest
struct HttpClientRequest_t078B6032A08DB03EEA44B54E49FE8A25D7DD04B7 
{
	// Realms.Native.HttpClientTransport/NativeHttpMethod Realms.Native.HttpClientTransport/HttpClientRequest::method
	int32_t ___method_0;
	// Realms.Native.PrimitiveValue Realms.Native.HttpClientTransport/HttpClientRequest::url
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___url_1;
	// System.UInt64 Realms.Native.HttpClientTransport/HttpClientRequest::timeout_ms
	uint64_t ___timeout_ms_2;
	// System.IntPtr Realms.Native.HttpClientTransport/HttpClientRequest::headers
	intptr_t ___headers_3;
	// System.IntPtr Realms.Native.HttpClientTransport/HttpClientRequest::headers_len
	intptr_t ___headers_len_4;
	// Realms.Native.PrimitiveValue Realms.Native.HttpClientTransport/HttpClientRequest::body
	PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___body_5;
	// System.IntPtr Realms.Native.HttpClientTransport/HttpClientRequest::managed_http_client
	intptr_t ___managed_http_client_6;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

extern "C" void CDECL ReversePInvokeWrapper_AppHandle_HandleApiKeysCallback_mB2501BCB407DE19C274BD21F346BD267327F78E2(intptr_t ___tcs_ptr0, intptr_t ___api_keys1, intptr_t ___api_keys_len2, AppError_t76E59E2102C582D663A3D26011D7CA63B7759946 ___error3);
extern "C" void CDECL ReversePInvokeWrapper_AppHandle_HandleStringCallback_mA05845D68CB04777DE2B49EFDE2D56BFB4C4D146(intptr_t ___tcs_ptr0, PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___response1, AppError_t76E59E2102C582D663A3D26011D7CA63B7759946 ___error2);
extern "C" void CDECL ReversePInvokeWrapper_AppHandle_HandleTaskCompletion_mDC61FC841FCA22BD0392595DC5536C91D62F509B(intptr_t ___tcs_ptr0, AppError_t76E59E2102C582D663A3D26011D7CA63B7759946 ___error1);
extern "C" void CDECL ReversePInvokeWrapper_AppHandle_HandleUserCallback_m421AC6BD85022205AC7B4E72A72A7ACC5E6D96EF(intptr_t ___tcs_ptr0, intptr_t ___user_ptr1, AppError_t76E59E2102C582D663A3D26011D7CA63B7759946 ___error2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_CultureInfo_OnCultureInfoChangedInAppX_mDBD419B094B2CFE933BB3F63886A5AB4E44D2DC0(Il2CppChar* ___language0);
extern "C" int32_t CDECL ReversePInvokeWrapper_DeflateStreamNative_UnmanagedRead_m79E9628F6FF91A3995491997242DD83046C9B704(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" int32_t CDECL ReversePInvokeWrapper_DeflateStreamNative_UnmanagedWrite_m509AE153ECB916CDF755432335FBAB2B51CA1486(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" void CDECL ReversePInvokeWrapper_DictionaryHandle_NotifyDictionaryChanged_m288E56896EC5E60961141379CFF159E37D5E84C8(intptr_t ___managedHandle0, intptr_t ___changes1, uint8_t ___shallow2);
extern "C" void CDECL ReversePInvokeWrapper_HttpClientTransport_ExecuteRequest_m149C90C3CAA57EC29242C2ACC8ABAEEAE1BAA92A(HttpClientRequest_t078B6032A08DB03EEA44B54E49FE8A25D7DD04B7 ___request0, intptr_t ___callback1);
extern "C" void CDECL ReversePInvokeWrapper_NotifiableObjectHandleBase_NotifyObjectChanged_m6EA82206D6CEF01D15F12720D18A1DE3D0FFD63A(intptr_t ___managedHandle0, intptr_t ___changes1, int32_t ___shallow2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_mB85BF0265E239960FC963DCA74DC67EBCE9480AC(intptr_t ___arg0);
extern "C" void CDECL ReversePInvokeWrapper_SessionHandle_HandleSessionError_m667E18D43C5F5AE52227C8C72096E2F119E1DAC9(intptr_t ___sessionHandlePtr0, SyncError_tE2E8948799F75F68D3627FD39942895E146334E3 ___error1, intptr_t ___managedSyncConfigurationBaseHandle2);
extern "C" void CDECL ReversePInvokeWrapper_SessionHandle_HandleSessionProgress_m812A2F334ACCEDD8CD5F7B31B9140362CAFDCD83(intptr_t ___tokenPtr0, uint64_t ___transferredBytes1, uint64_t ___transferableBytes2);
extern "C" void CDECL ReversePInvokeWrapper_SessionHandle_HandleSessionPropertyChangedCallback_m4DE63D7378FD1169ADEA7D961E2B6E13BDE6438F(intptr_t ___managedSessionHandle0, uint8_t ___property1);
extern "C" void CDECL ReversePInvokeWrapper_SessionHandle_HandleSessionWaitCallback_mA2465E6F4774DF8B72811D5FA960DD7870281C43(intptr_t ___taskCompletionSource0, int32_t ___error_code1, PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___message2);
extern "C" intptr_t CDECL ReversePInvokeWrapper_SessionHandle_NotifyAfterClientReset_m738E6508060C1950EC05F1DBCF10462C0CA6049A(intptr_t ___beforeFrozen0, intptr_t ___after1, intptr_t ___managedSyncConfigurationHandle2, int32_t ___didRecover3);
extern "C" intptr_t CDECL ReversePInvokeWrapper_SessionHandle_NotifyBeforeClientReset_m6AC4FE67684BAE6DC50495933452445088C28925(intptr_t ___beforeFrozen0, intptr_t ___managedSyncConfigurationHandle1);
extern "C" void CDECL ReversePInvokeWrapper_SharedRealmHandle_DisposeGCHandleCallback_m56BEA952C1074A7AC415AB9A108484EE1203D9AD(intptr_t ___handle0);
extern "C" void CDECL ReversePInvokeWrapper_SharedRealmHandle_GetNativeSchema_m368009B9D4493B9C0702323C257B1870E58CB2CC(Schema_t119F81EC190B6E7B0C4776F0D55C0DC834A2E840 ___schema0, intptr_t ___managedCallbackPtr1);
extern "C" void CDECL ReversePInvokeWrapper_SharedRealmHandle_HandleOpenRealmCallback_m369FAD1E6528E79DFE62B70F137A8F9F0D647534(intptr_t ___taskCompletionSource0, intptr_t ___realm_reference1, NativeException_tD0D4AE8BF0E1593EA969F3725B458DDA9B8D25A7 ___ex2);
extern "C" void CDECL ReversePInvokeWrapper_SharedRealmHandle_HandleTaskCompletionCallback_m4D882F6F753CD48809D1E6B11D468DC5078D5EC5(intptr_t ___tcs_ptr0, uint8_t ___invoke_async1, NativeException_tD0D4AE8BF0E1593EA969F3725B458DDA9B8D25A7 ___ex2);
extern "C" void CDECL ReversePInvokeWrapper_SharedRealmHandle_LogMessage_m58FFB6109E08ADCD6C6967ED3BC376851DB8E8C0(StringValue_tA17D7236E5C9F0E6D7A77F144E825D848CA568B8 ___message0, int32_t ___level1);
extern "C" void CDECL ReversePInvokeWrapper_SharedRealmHandle_NotifyRealmChanged_m73BAC72C46F07A44FEE0FF2AF8A189644EC2A109(intptr_t ___stateHandle0);
extern "C" intptr_t CDECL ReversePInvokeWrapper_SharedRealmHandle_OnDataInitialization_mBD09B07460144699D4F36AB50D7AE0EEF4226055(intptr_t ___managedConfigHandle0, intptr_t ___realmPtr1);
extern "C" intptr_t CDECL ReversePInvokeWrapper_SharedRealmHandle_OnMigration_m151687D0F0B48F7B71D7FAEB5BB46AA66CAE0341(intptr_t ___oldRealmPtr0, intptr_t ___newRealmPtr1, intptr_t ___migrationSchema2, Schema_t119F81EC190B6E7B0C4776F0D55C0DC834A2E840 ___oldSchema3, uint64_t ___schemaVersion4, intptr_t ___managedConfigHandle5);
extern "C" intptr_t CDECL ReversePInvokeWrapper_SharedRealmHandle_ShouldCompactOnLaunchCallback_mD1214FC585B67572A87AB11154D0C1FE5390B185(intptr_t ___managedConfigHandle0, uint64_t ___totalSize1, uint64_t ___dataSize2, uint8_t* ___shouldCompact3);
extern "C" void CDECL ReversePInvokeWrapper_SubscriptionSetHandle_HandleStateWaitCallback_m0FF5906D74E121F4D8EC6930B03CE7FC2F3434A9(intptr_t ___taskCompletionSource0, uint8_t ___state1, PrimitiveValue_t0B0F1BDA24EE2A5C70CE1D6C87EF2C8EE9FAC30B ___message2);
extern "C" void CDECL ReversePInvokeWrapper_SubscriptionSetHandle_OnGetSubscription_mDE761648C104472C4FFDD70B980D5510BFD6CAD3(intptr_t ___managedCallbackPtr0, Subscription_t71D228FFF062330388899D9060576B43B033D18E ___subscription1);
extern "C" intptr_t CDECL ReversePInvokeWrapper_SynchronizationContextScheduler_GetCurrentSynchronizationContext_mB51AB6EB2A341651B7D126C78DD589C971176A6B();
extern "C" uint8_t CDECL ReversePInvokeWrapper_SynchronizationContextScheduler_IsOnSynchronizationContext_mBD988717418E318FC252341989C2A1AC8AF40CF5(intptr_t ___context0, intptr_t ___targetContext1);
extern "C" void CDECL ReversePInvokeWrapper_SynchronizationContextScheduler_PostOnSynchronizationContext_m7C675E44C502226B16A0DFD260020670F9D61B7C(intptr_t ___context0, intptr_t ___user_data1);
extern "C" void CDECL ReversePInvokeWrapper_SynchronizationContextScheduler_ReleaseSynchronizationContext_mFDDFE459A45AF941FF19FADBFCCF497E7CCADE77(intptr_t ___context0);
extern "C" void CDECL ReversePInvokeWrapper_UnityTlsContext_CertificateCallback_mF5E626BA2545CFFA64428622678E409702C2045A(void* ___userData0, unitytls_tlsctx_tF8BBCBFE1E957B846442DED65ECB89BC5307DEAE* ___ctx1, uint8_t* ___cn2, intptr_t ___cnLen3, unitytls_x509name_t8A1108C917795D8FE946B50769ACE51489C7BF5D* ___caList4, intptr_t ___caListLen5, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17* ___chain6, unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2* ___key7, unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902* ___errorState8);
extern "C" intptr_t CDECL ReversePInvokeWrapper_UnityTlsContext_ReadCallback_m15F3A217E44D480CAED06EB5A88503FB6259D7EC(void* ___userData0, uint8_t* ___buffer1, intptr_t ___bufferLen2, unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902* ___errorState3);
extern "C" uint32_t CDECL ReversePInvokeWrapper_UnityTlsContext_VerifyCallback_m75D7C072718405EBBF8A2A9C794C4DDFB2595BD0(void* ___userData0, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 ___chain1, unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902* ___errorState2);
extern "C" intptr_t CDECL ReversePInvokeWrapper_UnityTlsContext_WriteCallback_m53DD70115C97432A676F3E437E8FB42F9FC068F5(void* ___userData0, uint8_t* ___data1, intptr_t ___bufferLen2, unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902* ___errorState3);
extern "C" uint32_t CDECL ReversePInvokeWrapper_UnityTlsProvider_x509verify_callback_m47823254E133F7304ABFAE5A1F0D218402BC5B45(void* ___userData0, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 ___cert1, uint32_t ___result2, unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902* ___errorState3);
extern "C" int32_t CDECL ReversePInvokeWrapper_BurstCompilerHelper_IsBurstEnabled_m5EFFD7E2094228A267B5DBC3F28B82A7FF53E733();


IL2CPP_EXTERN_C const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[];
const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[38] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AppHandle_HandleApiKeysCallback_mB2501BCB407DE19C274BD21F346BD267327F78E2),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AppHandle_HandleStringCallback_mA05845D68CB04777DE2B49EFDE2D56BFB4C4D146),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AppHandle_HandleTaskCompletion_mDC61FC841FCA22BD0392595DC5536C91D62F509B),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AppHandle_HandleUserCallback_m421AC6BD85022205AC7B4E72A72A7ACC5E6D96EF),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_CultureInfo_OnCultureInfoChangedInAppX_mDBD419B094B2CFE933BB3F63886A5AB4E44D2DC0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStreamNative_UnmanagedRead_m79E9628F6FF91A3995491997242DD83046C9B704),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStreamNative_UnmanagedWrite_m509AE153ECB916CDF755432335FBAB2B51CA1486),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DictionaryHandle_NotifyDictionaryChanged_m288E56896EC5E60961141379CFF159E37D5E84C8),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_HttpClientTransport_ExecuteRequest_m149C90C3CAA57EC29242C2ACC8ABAEEAE1BAA92A),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NotifiableObjectHandleBase_NotifyObjectChanged_m6EA82206D6CEF01D15F12720D18A1DE3D0FFD63A),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_mB85BF0265E239960FC963DCA74DC67EBCE9480AC),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SessionHandle_HandleSessionError_m667E18D43C5F5AE52227C8C72096E2F119E1DAC9),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SessionHandle_HandleSessionProgress_m812A2F334ACCEDD8CD5F7B31B9140362CAFDCD83),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SessionHandle_HandleSessionPropertyChangedCallback_m4DE63D7378FD1169ADEA7D961E2B6E13BDE6438F),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SessionHandle_HandleSessionWaitCallback_mA2465E6F4774DF8B72811D5FA960DD7870281C43),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SessionHandle_NotifyAfterClientReset_m738E6508060C1950EC05F1DBCF10462C0CA6049A),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SessionHandle_NotifyBeforeClientReset_m6AC4FE67684BAE6DC50495933452445088C28925),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SharedRealmHandle_DisposeGCHandleCallback_m56BEA952C1074A7AC415AB9A108484EE1203D9AD),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SharedRealmHandle_GetNativeSchema_m368009B9D4493B9C0702323C257B1870E58CB2CC),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SharedRealmHandle_HandleOpenRealmCallback_m369FAD1E6528E79DFE62B70F137A8F9F0D647534),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SharedRealmHandle_HandleTaskCompletionCallback_m4D882F6F753CD48809D1E6B11D468DC5078D5EC5),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SharedRealmHandle_LogMessage_m58FFB6109E08ADCD6C6967ED3BC376851DB8E8C0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SharedRealmHandle_NotifyRealmChanged_m73BAC72C46F07A44FEE0FF2AF8A189644EC2A109),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SharedRealmHandle_OnDataInitialization_mBD09B07460144699D4F36AB50D7AE0EEF4226055),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SharedRealmHandle_OnMigration_m151687D0F0B48F7B71D7FAEB5BB46AA66CAE0341),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SharedRealmHandle_ShouldCompactOnLaunchCallback_mD1214FC585B67572A87AB11154D0C1FE5390B185),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SubscriptionSetHandle_HandleStateWaitCallback_m0FF5906D74E121F4D8EC6930B03CE7FC2F3434A9),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SubscriptionSetHandle_OnGetSubscription_mDE761648C104472C4FFDD70B980D5510BFD6CAD3),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SynchronizationContextScheduler_GetCurrentSynchronizationContext_mB51AB6EB2A341651B7D126C78DD589C971176A6B),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SynchronizationContextScheduler_IsOnSynchronizationContext_mBD988717418E318FC252341989C2A1AC8AF40CF5),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SynchronizationContextScheduler_PostOnSynchronizationContext_m7C675E44C502226B16A0DFD260020670F9D61B7C),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SynchronizationContextScheduler_ReleaseSynchronizationContext_mFDDFE459A45AF941FF19FADBFCCF497E7CCADE77),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityTlsContext_CertificateCallback_mF5E626BA2545CFFA64428622678E409702C2045A),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityTlsContext_ReadCallback_m15F3A217E44D480CAED06EB5A88503FB6259D7EC),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityTlsContext_VerifyCallback_m75D7C072718405EBBF8A2A9C794C4DDFB2595BD0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityTlsContext_WriteCallback_m53DD70115C97432A676F3E437E8FB42F9FC068F5),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityTlsProvider_x509verify_callback_m47823254E133F7304ABFAE5A1F0D218402BC5B45),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BurstCompilerHelper_IsBurstEnabled_m5EFFD7E2094228A267B5DBC3F28B82A7FF53E733),
};

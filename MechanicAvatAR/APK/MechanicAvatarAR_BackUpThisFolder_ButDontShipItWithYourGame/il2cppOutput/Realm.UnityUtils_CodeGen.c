﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mAF326E4654CE0EC05C175FA25AA26C9C7D7C47A6 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
extern void NullableAttribute__ctor_m21AFDEA65B07A07C784FAA6B3C384A8AC8B0B3D8 (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte[])
extern void NullableAttribute__ctor_m6024791939B7D3155C13B8448B1070D9AC88AF43 (void);
// 0x00000004 System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
extern void NullableContextAttribute__ctor_m11F48B013A5A15D23A146D66B9FBEB1C2A89D31E (void);
// 0x00000005 System.String UnityUtils.FileHelper::GetStorageFolder()
extern void FileHelper_GetStorageFolder_mFF272D1E49B2867A6E5419AEB026A2CD4B28CA80 (void);
// 0x00000006 System.Void UnityUtils.Initializer::Initialize()
extern void Initializer_Initialize_m64F6A2544384357CC209125635CAD25349B8C41C (void);
// 0x00000007 System.Void UnityUtils.Initializer::OnSubsystemRegistration()
extern void Initializer_OnSubsystemRegistration_mCF4EE16D30684FE89437EB4F4E5784AB0CCFC823 (void);
// 0x00000008 System.Void UnityUtils.Initializer/<>c::.cctor()
extern void U3CU3Ec__cctor_m0510CAD7770206288BCEC22436D96B7BD1CAFBB3 (void);
// 0x00000009 System.Void UnityUtils.Initializer/<>c::.ctor()
extern void U3CU3Ec__ctor_m7466869C3B44B29E5141DBD25C4A6A92634CAA94 (void);
// 0x0000000A System.Void UnityUtils.Initializer/<>c::<Initialize>b__1_0()
extern void U3CU3Ec_U3CInitializeU3Eb__1_0_m40ED69A0F65E8E65B82105C755A0CF31D124ACDF (void);
// 0x0000000B System.Void UnityUtils.UnityLogger::LogImpl(Realms.Logging.LogLevel,System.String)
extern void UnityLogger_LogImpl_m6F7E2B3083805FAABD58C8F06F9D87FC6939D775 (void);
// 0x0000000C System.Void UnityUtils.UnityLogger::.ctor()
extern void UnityLogger__ctor_m113DDF803C03610FE8DFBFBD9CD005161C192926 (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	EmbeddedAttribute__ctor_mAF326E4654CE0EC05C175FA25AA26C9C7D7C47A6,
	NullableAttribute__ctor_m21AFDEA65B07A07C784FAA6B3C384A8AC8B0B3D8,
	NullableAttribute__ctor_m6024791939B7D3155C13B8448B1070D9AC88AF43,
	NullableContextAttribute__ctor_m11F48B013A5A15D23A146D66B9FBEB1C2A89D31E,
	FileHelper_GetStorageFolder_mFF272D1E49B2867A6E5419AEB026A2CD4B28CA80,
	Initializer_Initialize_m64F6A2544384357CC209125635CAD25349B8C41C,
	Initializer_OnSubsystemRegistration_mCF4EE16D30684FE89437EB4F4E5784AB0CCFC823,
	U3CU3Ec__cctor_m0510CAD7770206288BCEC22436D96B7BD1CAFBB3,
	U3CU3Ec__ctor_m7466869C3B44B29E5141DBD25C4A6A92634CAA94,
	U3CU3Ec_U3CInitializeU3Eb__1_0_m40ED69A0F65E8E65B82105C755A0CF31D124ACDF,
	UnityLogger_LogImpl_m6F7E2B3083805FAABD58C8F06F9D87FC6939D775,
	UnityLogger__ctor_m113DDF803C03610FE8DFBFBD9CD005161C192926,
};
static const int32_t s_InvokerIndices[12] = 
{
	10250,
	8401,
	8515,
	8401,
	16586,
	16632,
	16632,
	16632,
	10250,
	10250,
	4292,
	10250,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Realm_UnityUtils_CodeGenModule;
const Il2CppCodeGenModule g_Realm_UnityUtils_CodeGenModule = 
{
	"Realm.UnityUtils.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

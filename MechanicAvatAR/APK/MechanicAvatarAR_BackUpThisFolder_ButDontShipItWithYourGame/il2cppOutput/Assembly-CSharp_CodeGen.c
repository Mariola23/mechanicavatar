﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void <Module>::.cctor()
extern void U3CModuleU3E__cctor_m6AA90A27A1CC5904419E427E5F0AF2419FB211A9 (void);
// 0x00000002 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m68CAD82666F0FF415043D7DC217986AA2D3133D1 (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
extern void NullableAttribute__ctor_m771BA0E8EFE1DD8AAAE9E2B867CCF4D3AE8834C7 (void);
// 0x00000004 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte[])
extern void NullableAttribute__ctor_mA329224BEC75C65B8E9B5D81D7F5E769E22790E2 (void);
// 0x00000005 System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
extern void NullableContextAttribute__ctor_m3F94BA00FB614574AC19D78E61DC0CA0AE15FCAC (void);
// 0x00000006 System.Void AutoPlay::Start()
extern void AutoPlay_Start_mB70D4770E116E6CF9F59384ACADD89A37DC62D20 (void);
// 0x00000007 System.Void AutoPlay::Update()
extern void AutoPlay_Update_mDFBBCA2D848E04E091E09E130B0443F0FC9B4428 (void);
// 0x00000008 System.Void AutoPlay::.ctor()
extern void AutoPlay__ctor_m0048F0EBF8281CE23AB62A575A8C7D2F87A3F381 (void);
// 0x00000009 System.Void Buttons::cambiarPanelCrear()
extern void Buttons_cambiarPanelCrear_m54E8789F46E29F101B052A9DFABA723119C6187E (void);
// 0x0000000A System.Void Buttons::cambiarPanelCambiar()
extern void Buttons_cambiarPanelCambiar_m144E73928CE495F8B543A81DEA2A8488A0C56E22 (void);
// 0x0000000B System.Void Buttons::atras()
extern void Buttons_atras_mB833B6A3DEAEC49EE69A51D0B89AEA7374A01D1B (void);
// 0x0000000C System.Void Buttons::iniciarSesion()
extern void Buttons_iniciarSesion_m9CD78D1E0DACA323E5168C29D95FA41C851FA8EA (void);
// 0x0000000D System.Void Buttons::crearUsuario()
extern void Buttons_crearUsuario_m8D15452FD5C8168BEA9C5A3D38452884E1540EA0 (void);
// 0x0000000E System.Void Buttons::comprobarCambioContrase?a()
extern void Buttons_comprobarCambioContraseUF1a_m814DA4BD0F248F91174C19DF261CA2E5016A7644 (void);
// 0x0000000F System.Void Buttons::cambiarContrase?a()
extern void Buttons_cambiarContraseUF1a_m3B0FC094E4934C5A41A6CA475CB6C6A49285A397 (void);
// 0x00000010 System.Void Buttons::entrarSinContrase?a()
extern void Buttons_entrarSinContraseUF1a_mD8E6CF1B104E92FD92A87D4BA81130DCE748221B (void);
// 0x00000011 System.Void Buttons::ver_Contrase?a(TMPro.TMP_InputField)
extern void Buttons_ver_ContraseUF1a_mE7B017EA5A8F3CDCC99B6715899A61CD7AE355C1 (void);
// 0x00000012 System.Void Buttons::.ctor()
extern void Buttons__ctor_m3217EFBF53FE9D652EF123BDA4A06F98E196441D (void);
// 0x00000013 System.Void Choque::Start()
extern void Choque_Start_m47A0EBF9644517E33267B617CC7244C1B3CAA9C4 (void);
// 0x00000014 System.Void Choque::OnCollisionEnter(UnityEngine.Collision)
extern void Choque_OnCollisionEnter_m49855E353909337CB23C00B96134ED2CC12ED65A (void);
// 0x00000015 System.Void Choque::.ctor()
extern void Choque__ctor_m264171FAB7A76BF84F6A30A1F13953C20C2B0FC8 (void);
// 0x00000016 System.Void DBController::Awake()
extern void DBController_Awake_mC48562E4FACF93B34F90719407E807D2F7238347 (void);
// 0x00000017 System.Void DBController::OnDisable()
extern void DBController_OnDisable_mA697D91D235947C8689C438559CEC10EBE01082C (void);
// 0x00000018 System.Boolean DBController::EncontrarUsuarioPorUsuario(System.String,System.String)
extern void DBController_EncontrarUsuarioPorUsuario_mBB17C207B506F6B0DF20F5D13B3FAFBC0F269629 (void);
// 0x00000019 User DBController::EncontrarUsuarioPorUsuarioYEmail(System.String,System.String)
extern void DBController_EncontrarUsuarioPorUsuarioYEmail_m145CB4F3E6F5CF9ADA876D140A67C9A525C54EAB (void);
// 0x0000001A System.Boolean DBController::EncontrarUsuarioPorEmail(System.String,System.String)
extern void DBController_EncontrarUsuarioPorEmail_mF90327C81005A6C33262FFE6D68B1EB9E9DB5C72 (void);
// 0x0000001B System.String DBController::CrearUsuario(System.String,System.String,System.String)
extern void DBController_CrearUsuario_mFF50076581C68F3229A8FCBD0E1A9D95E8E97EA0 (void);
// 0x0000001C System.String DBController::CambiarContrase?aUsuario(User,System.String)
extern void DBController_CambiarContraseUF1aUsuario_mAFBBCF4E664AF4D935548A48B985B1B0C56420B4 (void);
// 0x0000001D System.Void DBController::.ctor()
extern void DBController__ctor_m5982A081CC5AF75ECFCA9F08DA9709315CEA2618 (void);
// 0x0000001E System.Void DBController::<Awake>b__5_0()
extern void DBController_U3CAwakeU3Eb__5_0_m170FC33CA6B9189156D3EC7355F50E5A72C0F0DF (void);
// 0x0000001F System.Void DBController/<Awake>d__5::MoveNext()
extern void U3CAwakeU3Ed__5_MoveNext_m9DEBB6C197CA30AD7C87C04EE9D62BD29CF59696 (void);
// 0x00000020 System.Void DBController/<Awake>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAwakeU3Ed__5_SetStateMachine_mAA66E47DAB6D49BEC13553F63103D53718989F57 (void);
// 0x00000021 System.Void DBController/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m32C097C8D3927D19E6D0B33A966D3E886E4FC60F (void);
// 0x00000022 System.Void DBController/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m27CDD30C1AE8C68FD8CC34BBB44EA23F5C0B56C2 (void);
// 0x00000023 System.Void DBController/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m1ABB54F977A1A621A10E53AD4B26B57D4112F104 (void);
// 0x00000024 System.Void DBController/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m2092F92D6819928C01040455240945D7DEFB81E2 (void);
// 0x00000025 System.Void DBController/<>c__DisplayClass10_1::.ctor()
extern void U3CU3Ec__DisplayClass10_1__ctor_m2A5C21CCBAA4CE4E9ADB43D44D30916C44119E8F (void);
// 0x00000026 System.Void DBController/<>c__DisplayClass10_1::<CrearUsuario>b__2()
extern void U3CU3Ec__DisplayClass10_1_U3CCrearUsuarioU3Eb__2_mEDBF75D8F855D93B54C560DE448861F330E06620 (void);
// 0x00000027 System.Void DBController/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m06BE024DBF7BA500C7475D6954AFEF13961A7FD0 (void);
// 0x00000028 System.Void DBController/<>c__DisplayClass11_0::<CambiarContrase?aUsuario>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CCambiarContraseUF1aUsuarioU3Eb__0_m0E2296196C5D0C68A233CB0F377104E200572868 (void);
// 0x00000029 System.Void PlayVideo::stopPlay()
extern void PlayVideo_stopPlay_m9BE59571EDE6AFDC547C6A72BFC87CC7F71EF1BD (void);
// 0x0000002A System.Void PlayVideo::.ctor()
extern void PlayVideo__ctor_m12D7A6FF63237382D2F34B45F32BCAC81191FF98 (void);
// 0x0000002B System.Void PlayVideo::.cctor()
extern void PlayVideo__cctor_m75002B1E6552518DF44B60F1AD1D5C3311CFC005 (void);
// 0x0000002C System.Void Principio::Start()
extern void Principio_Start_m9C3E0ED7AD0286DA74184754ABE135AB0EAA24FD (void);
// 0x0000002D System.Void Principio::Update()
extern void Principio_Update_m13D0FB1B8B2E7699DA330F2C01D268788420F9C7 (void);
// 0x0000002E System.Void Principio::OnStartSecuence()
extern void Principio_OnStartSecuence_mBF596F2F80333266C69EA7F38ABA36AC75DF5377 (void);
// 0x0000002F System.Void Principio::OnNextMove()
extern void Principio_OnNextMove_m2378376F53320AC9E3B39E9833E2AFBF7D62DAE5 (void);
// 0x00000030 System.Void Principio::.ctor()
extern void Principio__ctor_mB4D0AFE192B15FD63F8CBE35301DEF7D9D40B4EF (void);
// 0x00000031 System.Void SnapChange::Cambio()
extern void SnapChange_Cambio_mA55152BF0BDE876B55888AEC013D73330A70F033 (void);
// 0x00000032 System.Void SnapChange::.ctor()
extern void SnapChange__ctor_m9E15431F272A0917240072ABE32DDDCEA934C0E0 (void);
// 0x00000033 MongoDB.Bson.ObjectId User::get_Id()
extern void User_get_Id_m557E0A6983F4B322084CDC09979DDC1913FF66AE (void);
// 0x00000034 System.Void User::set_Id(MongoDB.Bson.ObjectId)
extern void User_set_Id_mE600BFBFBA6B043DCE3EDBC8E2AF4B818C7568F5 (void);
// 0x00000035 System.String User::get_Email()
extern void User_get_Email_m88CF4AB1DAE33A9452CE8A45EC28A620391DE6F8 (void);
// 0x00000036 System.Void User::set_Email(System.String)
extern void User_set_Email_mF6AA4CDF2DF87668E2F4C47F5490563E1933619A (void);
// 0x00000037 System.String User::get_UserName()
extern void User_get_UserName_mC8FFB977B9C625F062C859415E4BD356B7F9C193 (void);
// 0x00000038 System.Void User::set_UserName(System.String)
extern void User_set_UserName_mB71210FB11CF6055688B95A5BDF211408AB9EA22 (void);
// 0x00000039 System.String User::get_UserPass()
extern void User_get_UserPass_m028622ED2EBEE91AE8046D9204A0C93CEC70601C (void);
// 0x0000003A System.Void User::set_UserPass(System.String)
extern void User_set_UserPass_mB5F30320C475D01D9789FDE44BB432DC4DB19F4E (void);
// 0x0000003B System.Void User::.ctor(System.String,System.String,System.String)
extern void User__ctor_m1B1BAB8758B1E5F95627EF762681DBE0217D8E0C (void);
// 0x0000003C System.Void User::.ctor()
extern void User__ctor_mA93BAFF15F26978414F373DFC6287FA047561577 (void);
// 0x0000003D Realms.IRealmAccessor User::Realms.IRealmObjectBase.get_Accessor()
extern void User_Realms_IRealmObjectBase_get_Accessor_m12CBCE3BD49B16AE3DE40778C45DEBDB71DB1892 (void);
// 0x0000003E User/IUserAccessor User::get_Accessor()
extern void User_get_Accessor_mCA4F143FC6E898548A360714685D090FDEC942D3 (void);
// 0x0000003F System.Boolean User::get_IsManaged()
extern void User_get_IsManaged_m8AD01061AB39063F5AD1DEF0B828E90366A850D1 (void);
// 0x00000040 System.Boolean User::get_IsValid()
extern void User_get_IsValid_mD90AB0F8FF7E6BF631CFCF207D2109EBE46F0ADB (void);
// 0x00000041 System.Boolean User::get_IsFrozen()
extern void User_get_IsFrozen_mBA98F094438D6A75D7D7DFFD1B6331A3B20D3C1C (void);
// 0x00000042 Realms.Realm User::get_Realm()
extern void User_get_Realm_m2AF41555080EB69C93B6B46F0BE398D4CEF77731 (void);
// 0x00000043 Realms.Schema.ObjectSchema User::get_ObjectSchema()
extern void User_get_ObjectSchema_m42AB6E21CD662155FAB4E49D95DB59AA04FEDEEB (void);
// 0x00000044 Realms.DynamicObjectApi User::get_DynamicApi()
extern void User_get_DynamicApi_m0A89ECF74F20B3AE3E689AD44997D9D6EABF0862 (void);
// 0x00000045 System.Int32 User::get_BacklinksCount()
extern void User_get_BacklinksCount_m1AE1CAA07F2F28CD49CB9D10119E6038C082B620 (void);
// 0x00000046 System.Void User::Realms.ISettableManagedAccessor.SetManagedAccessor(Realms.IRealmAccessor,Realms.Weaving.IRealmObjectHelper,System.Boolean,System.Boolean)
extern void User_Realms_ISettableManagedAccessor_SetManagedAccessor_m9D62017934A2C00CED0A1895974995D6445C4294 (void);
// 0x00000047 System.Void User::add__propertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void User_add__propertyChanged_mE6C054B3EA93CD526DD311F372A61F4F22022290 (void);
// 0x00000048 System.Void User::remove__propertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void User_remove__propertyChanged_m8E9F5D64F01E2AFB8440B07DCA819F7A9F1A2112 (void);
// 0x00000049 System.Void User::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void User_add_PropertyChanged_mE1B02CDD147F7851DB93D8A47898F1F6DFAEB6FE (void);
// 0x0000004A System.Void User::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void User_remove_PropertyChanged_mD2B57E0850EE35E3BD6E77A4B82F27059F64AD27 (void);
// 0x0000004B System.Void User::RaisePropertyChanged(System.String)
extern void User_RaisePropertyChanged_m0AA38E8DAF800A2B858528529D30419B96B394A3 (void);
// 0x0000004C System.Void User::SubscribeForNotifications()
extern void User_SubscribeForNotifications_mE4C4005ED50DE3616FE0A5B1B3286750B527E2C3 (void);
// 0x0000004D System.Void User::UnsubscribeFromNotifications()
extern void User_UnsubscribeFromNotifications_m0495E735C3B6378BADA976EC03BAF0D1CEAAB9EF (void);
// 0x0000004E User User::op_Explicit(Realms.RealmValue)
extern void User_op_Explicit_mAA1ECB308104CF54010FCBDCF267C91F8F742063 (void);
// 0x0000004F Realms.RealmValue User::op_Implicit(User)
extern void User_op_Implicit_m4B230B18890C9F0B95A59425036E9A5D265EDAF2 (void);
// 0x00000050 System.Reflection.TypeInfo User::GetTypeInfo()
extern void User_GetTypeInfo_mA6A8C2C5837F89043C5DB5E9A2BEEB7C444D4541 (void);
// 0x00000051 System.Boolean User::Equals(System.Object)
extern void User_Equals_mDF01803BBA7FD83004E07CF2FFE197964D10599F (void);
// 0x00000052 System.Int32 User::GetHashCode()
extern void User_GetHashCode_mF7BDD6792F4AE20B580FE4CD464F06E6994949B5 (void);
// 0x00000053 System.String User::ToString()
extern void User_ToString_m2783639D63A3A5ED6A18FA475E2105C2EAAD5B7E (void);
// 0x00000054 System.Void User::.cctor()
extern void User__cctor_mDD4F890270946AC9AD9905E28E241BB7CD58528C (void);
// 0x00000055 System.Void User/UserObjectHelper::CopyToRealm(Realms.IRealmObjectBase,System.Boolean,System.Boolean)
extern void UserObjectHelper_CopyToRealm_m523B3D8E7977E65D33CD66E78B683AD20FF06085 (void);
// 0x00000056 Realms.ManagedAccessor User/UserObjectHelper::CreateAccessor()
extern void UserObjectHelper_CreateAccessor_mF2DFCFAACF7F8159F67AF1A8EFB4AE3AF57E203A (void);
// 0x00000057 Realms.IRealmObjectBase User/UserObjectHelper::CreateInstance()
extern void UserObjectHelper_CreateInstance_m2CEC0FB46CAB66C1F7D7DB8505EADC12A7C68D33 (void);
// 0x00000058 System.Boolean User/UserObjectHelper::TryGetPrimaryKeyValue(Realms.IRealmObjectBase,Realms.RealmValue&)
extern void UserObjectHelper_TryGetPrimaryKeyValue_m9938700ADD3BD9F0F661842E300D5CAE30BCE370 (void);
// 0x00000059 System.Void User/UserObjectHelper::.ctor()
extern void UserObjectHelper__ctor_mAA4765896C4EA9C0C941237751D3A51FDA340FDA (void);
// 0x0000005A MongoDB.Bson.ObjectId User/IUserAccessor::get_Id()
// 0x0000005B System.Void User/IUserAccessor::set_Id(MongoDB.Bson.ObjectId)
// 0x0000005C System.String User/IUserAccessor::get_Email()
// 0x0000005D System.Void User/IUserAccessor::set_Email(System.String)
// 0x0000005E System.String User/IUserAccessor::get_UserName()
// 0x0000005F System.Void User/IUserAccessor::set_UserName(System.String)
// 0x00000060 System.String User/IUserAccessor::get_UserPass()
// 0x00000061 System.Void User/IUserAccessor::set_UserPass(System.String)
// 0x00000062 MongoDB.Bson.ObjectId User/UserManagedAccessor::get_Id()
extern void UserManagedAccessor_get_Id_m4FDF78B1E78092B3E125D5BA292B22C9C1960480 (void);
// 0x00000063 System.Void User/UserManagedAccessor::set_Id(MongoDB.Bson.ObjectId)
extern void UserManagedAccessor_set_Id_m155B00731406CF29B15F4818687DE86D3B187396 (void);
// 0x00000064 System.String User/UserManagedAccessor::get_Email()
extern void UserManagedAccessor_get_Email_m0016AC8779C16A417A226C5212CBD86223607EF8 (void);
// 0x00000065 System.Void User/UserManagedAccessor::set_Email(System.String)
extern void UserManagedAccessor_set_Email_m3F881AE088FCE6507BB7D712DEFFFFF7CCA9C3B4 (void);
// 0x00000066 System.String User/UserManagedAccessor::get_UserName()
extern void UserManagedAccessor_get_UserName_m740F8387A41A8E721BCE480D80F5F9C68846E060 (void);
// 0x00000067 System.Void User/UserManagedAccessor::set_UserName(System.String)
extern void UserManagedAccessor_set_UserName_m7B088714569B80CE4EBD278F172F2047BA0BFA18 (void);
// 0x00000068 System.String User/UserManagedAccessor::get_UserPass()
extern void UserManagedAccessor_get_UserPass_m687D18AEF58374C9303D1345F88372426AD937E2 (void);
// 0x00000069 System.Void User/UserManagedAccessor::set_UserPass(System.String)
extern void UserManagedAccessor_set_UserPass_mC458D18C8322472812B4DFC102E59B618F415B4A (void);
// 0x0000006A System.Void User/UserManagedAccessor::.ctor()
extern void UserManagedAccessor__ctor_m83E59932A712B2EE2BFD48FBFC81B8780959DCF6 (void);
// 0x0000006B Realms.Schema.ObjectSchema User/UserUnmanagedAccessor::get_ObjectSchema()
extern void UserUnmanagedAccessor_get_ObjectSchema_mA3EAB61B5ACE937225C2C6483D3C8AC77D03CC32 (void);
// 0x0000006C MongoDB.Bson.ObjectId User/UserUnmanagedAccessor::get_Id()
extern void UserUnmanagedAccessor_get_Id_m3FC9EFFC660EB3B212F25F924B14FC732F558DBC (void);
// 0x0000006D System.Void User/UserUnmanagedAccessor::set_Id(MongoDB.Bson.ObjectId)
extern void UserUnmanagedAccessor_set_Id_m821FCB6844F6EF34A67B883A017694E3B6D6D3E3 (void);
// 0x0000006E System.String User/UserUnmanagedAccessor::get_Email()
extern void UserUnmanagedAccessor_get_Email_m5768CA5CCD06AC94A2E772F1DCA3B8BDC92E8DAB (void);
// 0x0000006F System.Void User/UserUnmanagedAccessor::set_Email(System.String)
extern void UserUnmanagedAccessor_set_Email_mAF818E3725A01C68F32DF7552D11924CD8BD9AEB (void);
// 0x00000070 System.String User/UserUnmanagedAccessor::get_UserName()
extern void UserUnmanagedAccessor_get_UserName_m5A733CC42C49151FEB2C149839E393CA627F4817 (void);
// 0x00000071 System.Void User/UserUnmanagedAccessor::set_UserName(System.String)
extern void UserUnmanagedAccessor_set_UserName_m67D3D7A2E5B0BEB1CBF4B51F8E6B80EC6FDD1DA3 (void);
// 0x00000072 System.String User/UserUnmanagedAccessor::get_UserPass()
extern void UserUnmanagedAccessor_get_UserPass_m534E86C59A4B8617E80D40DF02D2E4D5995854D1 (void);
// 0x00000073 System.Void User/UserUnmanagedAccessor::set_UserPass(System.String)
extern void UserUnmanagedAccessor_set_UserPass_m3124499B5CCF2366F4ED7CCBEE0777EEF0EC41E2 (void);
// 0x00000074 System.Void User/UserUnmanagedAccessor::.ctor(System.Type)
extern void UserUnmanagedAccessor__ctor_m454082CE173557E06D236169234D2EC2A5F367C2 (void);
// 0x00000075 Realms.RealmValue User/UserUnmanagedAccessor::GetValue(System.String)
extern void UserUnmanagedAccessor_GetValue_mE8190C05210C5D2E27BFC629A6FCBC69588201EF (void);
// 0x00000076 System.Void User/UserUnmanagedAccessor::SetValue(System.String,Realms.RealmValue)
extern void UserUnmanagedAccessor_SetValue_m2E8C83A05748B8E33C2AFA8781A90DF2ACADC718 (void);
// 0x00000077 System.Void User/UserUnmanagedAccessor::SetValueUnique(System.String,Realms.RealmValue)
extern void UserUnmanagedAccessor_SetValueUnique_m12BA93DBAA708BD9364A4A192E4AEFFE149F3DD8 (void);
// 0x00000078 System.Collections.Generic.IList`1<T> User/UserUnmanagedAccessor::GetListValue(System.String)
// 0x00000079 System.Collections.Generic.ISet`1<T> User/UserUnmanagedAccessor::GetSetValue(System.String)
// 0x0000007A System.Collections.Generic.IDictionary`2<System.String,TValue> User/UserUnmanagedAccessor::GetDictionaryValue(System.String)
// 0x0000007B MongoDB.Bson.ObjectId UserSession::get_Id()
extern void UserSession_get_Id_m5A8577B87A7C08C2519E6360FF5FB00BB7004B5F (void);
// 0x0000007C System.Void UserSession::set_Id(MongoDB.Bson.ObjectId)
extern void UserSession_set_Id_m3057DC3BAF8BF17874C8FBEE4BA6F82970C7CF40 (void);
// 0x0000007D System.DateTimeOffset UserSession::get_LastLoginDate()
extern void UserSession_get_LastLoginDate_m17F3AC0D1861FFB1EC85392051329B984AFE88B0 (void);
// 0x0000007E System.Void UserSession::set_LastLoginDate(System.DateTimeOffset)
extern void UserSession_set_LastLoginDate_m54A0C24A2C42751B580299AF2C3FB558B4C87E83 (void);
// 0x0000007F System.String UserSession::get_SessionData()
extern void UserSession_get_SessionData_m8446890C4B6F01B6F806115B4261CD1FECA94EA0 (void);
// 0x00000080 System.Void UserSession::set_SessionData(System.String)
extern void UserSession_set_SessionData_m8FCC7BAF575446EC54AF905C6763F3E0375D491A (void);
// 0x00000081 User UserSession::get_User()
extern void UserSession_get_User_mDA7324975F5D05C7045B1D16403ADEE63F8F797F (void);
// 0x00000082 System.Void UserSession::set_User(User)
extern void UserSession_set_User_m65A782DC8F9817405E216FDBD6EA141562AF405D (void);
// 0x00000083 Realms.IRealmAccessor UserSession::Realms.IRealmObjectBase.get_Accessor()
extern void UserSession_Realms_IRealmObjectBase_get_Accessor_m6120DCC6D67C3BAF4FFF807A938BA9C26630E478 (void);
// 0x00000084 UserSession/IUserSessionAccessor UserSession::get_Accessor()
extern void UserSession_get_Accessor_m63953B090B87E08BA1BFE7A44D1E8489D7DBC32F (void);
// 0x00000085 System.Boolean UserSession::get_IsManaged()
extern void UserSession_get_IsManaged_m11FD78901058CAE201AF02C39887433CE17CA857 (void);
// 0x00000086 System.Boolean UserSession::get_IsValid()
extern void UserSession_get_IsValid_mBB0774B2D4504CCC1177CA74CDDEE6C38BA0359C (void);
// 0x00000087 System.Boolean UserSession::get_IsFrozen()
extern void UserSession_get_IsFrozen_m83CA809D954798CC7B0CD912932252846BF4FBE7 (void);
// 0x00000088 Realms.Realm UserSession::get_Realm()
extern void UserSession_get_Realm_m14DC5FB29C73D12D11E6BA8EA3687FB8DCC9445F (void);
// 0x00000089 Realms.Schema.ObjectSchema UserSession::get_ObjectSchema()
extern void UserSession_get_ObjectSchema_mD3DF49AD07270FBBD32177E92F1BBA639084F1A2 (void);
// 0x0000008A Realms.DynamicObjectApi UserSession::get_DynamicApi()
extern void UserSession_get_DynamicApi_mF0EF64999294058BF42D7BC3A81B98B3D498DF3D (void);
// 0x0000008B System.Int32 UserSession::get_BacklinksCount()
extern void UserSession_get_BacklinksCount_m260E267AF92CC769D011F20BCA8C1B6F3ED2C6EC (void);
// 0x0000008C System.Void UserSession::Realms.ISettableManagedAccessor.SetManagedAccessor(Realms.IRealmAccessor,Realms.Weaving.IRealmObjectHelper,System.Boolean,System.Boolean)
extern void UserSession_Realms_ISettableManagedAccessor_SetManagedAccessor_m092E0770BBEC403983296C41E65316C4045EC0CF (void);
// 0x0000008D System.Void UserSession::add__propertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void UserSession_add__propertyChanged_m7B26F46CF4984EEF7452F27585D49B8942ED5218 (void);
// 0x0000008E System.Void UserSession::remove__propertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void UserSession_remove__propertyChanged_mBC93A1936D8A602849EC8ACC0D53BA462DC1DAB7 (void);
// 0x0000008F System.Void UserSession::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void UserSession_add_PropertyChanged_m0F7648219EBFCD428C9AD7AA8A9559D3CF0942C0 (void);
// 0x00000090 System.Void UserSession::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void UserSession_remove_PropertyChanged_m19DA0F3754D3EFA21831309CD945B4319E7F6DDB (void);
// 0x00000091 System.Void UserSession::RaisePropertyChanged(System.String)
extern void UserSession_RaisePropertyChanged_m46270269687EA818A8E551C31698863FAF628078 (void);
// 0x00000092 System.Void UserSession::SubscribeForNotifications()
extern void UserSession_SubscribeForNotifications_m5CCE8F8BD5EFC9A4DEA465703C44BCAA24A4C22D (void);
// 0x00000093 System.Void UserSession::UnsubscribeFromNotifications()
extern void UserSession_UnsubscribeFromNotifications_m760D5BD91807904327064F1CF877E3B22524B888 (void);
// 0x00000094 UserSession UserSession::op_Explicit(Realms.RealmValue)
extern void UserSession_op_Explicit_mF91738A2C5184678B4FEF6B8E3A3B552C227BE63 (void);
// 0x00000095 Realms.RealmValue UserSession::op_Implicit(UserSession)
extern void UserSession_op_Implicit_m1C20940A351D7EDAADA8F85E0CE51E661077F971 (void);
// 0x00000096 System.Reflection.TypeInfo UserSession::GetTypeInfo()
extern void UserSession_GetTypeInfo_m968F0E52173B072ED9053E380186B1C30FECFE6D (void);
// 0x00000097 System.Boolean UserSession::Equals(System.Object)
extern void UserSession_Equals_m891833BCA3C7336A98CF4B0E9889D06649708BB1 (void);
// 0x00000098 System.Int32 UserSession::GetHashCode()
extern void UserSession_GetHashCode_m3D1A0624389A32DB13B8CCF26D71AE97E73AE7B7 (void);
// 0x00000099 System.String UserSession::ToString()
extern void UserSession_ToString_m6F1A5F11605065424F9D16FE5D95A2D47D3EF077 (void);
// 0x0000009A System.Void UserSession::.ctor()
extern void UserSession__ctor_m342672E88DD19210377EA9A037DBD93506AE8B1E (void);
// 0x0000009B System.Void UserSession::.cctor()
extern void UserSession__cctor_m681ABE13EA8E4EA6992AC1B393817E9F638F5301 (void);
// 0x0000009C System.Void UserSession/UserSessionObjectHelper::CopyToRealm(Realms.IRealmObjectBase,System.Boolean,System.Boolean)
extern void UserSessionObjectHelper_CopyToRealm_m45387BA7C5A6F0DCCF437651D6421107A8852189 (void);
// 0x0000009D Realms.ManagedAccessor UserSession/UserSessionObjectHelper::CreateAccessor()
extern void UserSessionObjectHelper_CreateAccessor_m755FEFBCEA322EB3C95E51BE23FE8C809CCF6E01 (void);
// 0x0000009E Realms.IRealmObjectBase UserSession/UserSessionObjectHelper::CreateInstance()
extern void UserSessionObjectHelper_CreateInstance_mC4533DB4D4D8080E8845CC81559F9D35642A3E03 (void);
// 0x0000009F System.Boolean UserSession/UserSessionObjectHelper::TryGetPrimaryKeyValue(Realms.IRealmObjectBase,Realms.RealmValue&)
extern void UserSessionObjectHelper_TryGetPrimaryKeyValue_m789D4F4B95FD57107244569E8487789E2EB5EF75 (void);
// 0x000000A0 System.Void UserSession/UserSessionObjectHelper::.ctor()
extern void UserSessionObjectHelper__ctor_m0586222A5538FD81F65F370FFF21B9C61FF42A57 (void);
// 0x000000A1 MongoDB.Bson.ObjectId UserSession/IUserSessionAccessor::get_Id()
// 0x000000A2 System.Void UserSession/IUserSessionAccessor::set_Id(MongoDB.Bson.ObjectId)
// 0x000000A3 System.DateTimeOffset UserSession/IUserSessionAccessor::get_LastLoginDate()
// 0x000000A4 System.Void UserSession/IUserSessionAccessor::set_LastLoginDate(System.DateTimeOffset)
// 0x000000A5 System.String UserSession/IUserSessionAccessor::get_SessionData()
// 0x000000A6 System.Void UserSession/IUserSessionAccessor::set_SessionData(System.String)
// 0x000000A7 User UserSession/IUserSessionAccessor::get_User()
// 0x000000A8 System.Void UserSession/IUserSessionAccessor::set_User(User)
// 0x000000A9 MongoDB.Bson.ObjectId UserSession/UserSessionManagedAccessor::get_Id()
extern void UserSessionManagedAccessor_get_Id_m02468EB74C25DCFFE85B7D53F724E3BFA2AE63F9 (void);
// 0x000000AA System.Void UserSession/UserSessionManagedAccessor::set_Id(MongoDB.Bson.ObjectId)
extern void UserSessionManagedAccessor_set_Id_mDF5BAB86E262020C31F62C52D253D992A8D224A6 (void);
// 0x000000AB System.DateTimeOffset UserSession/UserSessionManagedAccessor::get_LastLoginDate()
extern void UserSessionManagedAccessor_get_LastLoginDate_m65C300B96CA995BDE13A13EEB7B93E74704EFFAA (void);
// 0x000000AC System.Void UserSession/UserSessionManagedAccessor::set_LastLoginDate(System.DateTimeOffset)
extern void UserSessionManagedAccessor_set_LastLoginDate_m2000A01F3F601FF5897A9B3C70EC468120FD70F5 (void);
// 0x000000AD System.String UserSession/UserSessionManagedAccessor::get_SessionData()
extern void UserSessionManagedAccessor_get_SessionData_m615DE3592BEA60F938138E9E25D423619BB79064 (void);
// 0x000000AE System.Void UserSession/UserSessionManagedAccessor::set_SessionData(System.String)
extern void UserSessionManagedAccessor_set_SessionData_m534FE5BCBFFF9560318CD2942D207740339254E4 (void);
// 0x000000AF User UserSession/UserSessionManagedAccessor::get_User()
extern void UserSessionManagedAccessor_get_User_m63321D619B109BB190495478D4B20D8FAE82B8E9 (void);
// 0x000000B0 System.Void UserSession/UserSessionManagedAccessor::set_User(User)
extern void UserSessionManagedAccessor_set_User_m5BE304F726171E1D5DE1ED49C46D9330919CAD4F (void);
// 0x000000B1 System.Void UserSession/UserSessionManagedAccessor::.ctor()
extern void UserSessionManagedAccessor__ctor_m6691455464325BC2AE2C8F3FD3949CD6AFDF20E5 (void);
// 0x000000B2 Realms.Schema.ObjectSchema UserSession/UserSessionUnmanagedAccessor::get_ObjectSchema()
extern void UserSessionUnmanagedAccessor_get_ObjectSchema_m528AEE81454EB2CC43DB2AB29003C2985D0E063F (void);
// 0x000000B3 MongoDB.Bson.ObjectId UserSession/UserSessionUnmanagedAccessor::get_Id()
extern void UserSessionUnmanagedAccessor_get_Id_m0AF90DEDC6F5472335B58494F63A5D1644463490 (void);
// 0x000000B4 System.Void UserSession/UserSessionUnmanagedAccessor::set_Id(MongoDB.Bson.ObjectId)
extern void UserSessionUnmanagedAccessor_set_Id_m1681EEF72CCF036668E12B563C3A93A4EF9B4C00 (void);
// 0x000000B5 System.DateTimeOffset UserSession/UserSessionUnmanagedAccessor::get_LastLoginDate()
extern void UserSessionUnmanagedAccessor_get_LastLoginDate_mE675EFAD7CD6803E2BF2ECA215FD8704DE228627 (void);
// 0x000000B6 System.Void UserSession/UserSessionUnmanagedAccessor::set_LastLoginDate(System.DateTimeOffset)
extern void UserSessionUnmanagedAccessor_set_LastLoginDate_m59AD4B470C87778D2D77589122492A3ED3BF8BA2 (void);
// 0x000000B7 System.String UserSession/UserSessionUnmanagedAccessor::get_SessionData()
extern void UserSessionUnmanagedAccessor_get_SessionData_m29D1C7D150888957797FA22DF52D96CDF774EE1E (void);
// 0x000000B8 System.Void UserSession/UserSessionUnmanagedAccessor::set_SessionData(System.String)
extern void UserSessionUnmanagedAccessor_set_SessionData_m20C48D76CF2722050B95D0B9AC192777C1741811 (void);
// 0x000000B9 User UserSession/UserSessionUnmanagedAccessor::get_User()
extern void UserSessionUnmanagedAccessor_get_User_mF0D715502E3DF88F932CB9BA7F158EE1A20538A1 (void);
// 0x000000BA System.Void UserSession/UserSessionUnmanagedAccessor::set_User(User)
extern void UserSessionUnmanagedAccessor_set_User_mD74F504365F14AFD7ECEB2DDFD9EFCAA3B7975DB (void);
// 0x000000BB System.Void UserSession/UserSessionUnmanagedAccessor::.ctor(System.Type)
extern void UserSessionUnmanagedAccessor__ctor_m611EC5D38B86B1B28C299CF9DADB60873A51A4F1 (void);
// 0x000000BC Realms.RealmValue UserSession/UserSessionUnmanagedAccessor::GetValue(System.String)
extern void UserSessionUnmanagedAccessor_GetValue_m6790CCED6618F6F32B4C61CAC7AFD3BCEA4E5581 (void);
// 0x000000BD System.Void UserSession/UserSessionUnmanagedAccessor::SetValue(System.String,Realms.RealmValue)
extern void UserSessionUnmanagedAccessor_SetValue_m0155D5E87C9E1EE5FFB25AD7C00CBA7A244D78EF (void);
// 0x000000BE System.Void UserSession/UserSessionUnmanagedAccessor::SetValueUnique(System.String,Realms.RealmValue)
extern void UserSessionUnmanagedAccessor_SetValueUnique_mA29B60DC46D42104D523EC91102C8B021659B3E1 (void);
// 0x000000BF System.Collections.Generic.IList`1<T> UserSession/UserSessionUnmanagedAccessor::GetListValue(System.String)
// 0x000000C0 System.Collections.Generic.ISet`1<T> UserSession/UserSessionUnmanagedAccessor::GetSetValue(System.String)
// 0x000000C1 System.Collections.Generic.IDictionary`2<System.String,TValue> UserSession/UserSessionUnmanagedAccessor::GetDictionaryValue(System.String)
// 0x000000C2 System.Void PropertyChanged.DoNotNotifyAttribute::.ctor()
extern void DoNotNotifyAttribute__ctor_m9B4E17379FBAB9411FFDCAC0493791E1D7E7F1FF (void);
// 0x000000C3 System.Void RealmModuleInitializer::Initialize()
extern void RealmModuleInitializer_Initialize_m34E16C73A76952675DAEE7A897E90E10AF58CAD9 (void);
static Il2CppMethodPointer s_methodPointers[195] = 
{
	U3CModuleU3E__cctor_m6AA90A27A1CC5904419E427E5F0AF2419FB211A9,
	EmbeddedAttribute__ctor_m68CAD82666F0FF415043D7DC217986AA2D3133D1,
	NullableAttribute__ctor_m771BA0E8EFE1DD8AAAE9E2B867CCF4D3AE8834C7,
	NullableAttribute__ctor_mA329224BEC75C65B8E9B5D81D7F5E769E22790E2,
	NullableContextAttribute__ctor_m3F94BA00FB614574AC19D78E61DC0CA0AE15FCAC,
	AutoPlay_Start_mB70D4770E116E6CF9F59384ACADD89A37DC62D20,
	AutoPlay_Update_mDFBBCA2D848E04E091E09E130B0443F0FC9B4428,
	AutoPlay__ctor_m0048F0EBF8281CE23AB62A575A8C7D2F87A3F381,
	Buttons_cambiarPanelCrear_m54E8789F46E29F101B052A9DFABA723119C6187E,
	Buttons_cambiarPanelCambiar_m144E73928CE495F8B543A81DEA2A8488A0C56E22,
	Buttons_atras_mB833B6A3DEAEC49EE69A51D0B89AEA7374A01D1B,
	Buttons_iniciarSesion_m9CD78D1E0DACA323E5168C29D95FA41C851FA8EA,
	Buttons_crearUsuario_m8D15452FD5C8168BEA9C5A3D38452884E1540EA0,
	Buttons_comprobarCambioContraseUF1a_m814DA4BD0F248F91174C19DF261CA2E5016A7644,
	Buttons_cambiarContraseUF1a_m3B0FC094E4934C5A41A6CA475CB6C6A49285A397,
	Buttons_entrarSinContraseUF1a_mD8E6CF1B104E92FD92A87D4BA81130DCE748221B,
	Buttons_ver_ContraseUF1a_mE7B017EA5A8F3CDCC99B6715899A61CD7AE355C1,
	Buttons__ctor_m3217EFBF53FE9D652EF123BDA4A06F98E196441D,
	Choque_Start_m47A0EBF9644517E33267B617CC7244C1B3CAA9C4,
	Choque_OnCollisionEnter_m49855E353909337CB23C00B96134ED2CC12ED65A,
	Choque__ctor_m264171FAB7A76BF84F6A30A1F13953C20C2B0FC8,
	DBController_Awake_mC48562E4FACF93B34F90719407E807D2F7238347,
	DBController_OnDisable_mA697D91D235947C8689C438559CEC10EBE01082C,
	DBController_EncontrarUsuarioPorUsuario_mBB17C207B506F6B0DF20F5D13B3FAFBC0F269629,
	DBController_EncontrarUsuarioPorUsuarioYEmail_m145CB4F3E6F5CF9ADA876D140A67C9A525C54EAB,
	DBController_EncontrarUsuarioPorEmail_mF90327C81005A6C33262FFE6D68B1EB9E9DB5C72,
	DBController_CrearUsuario_mFF50076581C68F3229A8FCBD0E1A9D95E8E97EA0,
	DBController_CambiarContraseUF1aUsuario_mAFBBCF4E664AF4D935548A48B985B1B0C56420B4,
	DBController__ctor_m5982A081CC5AF75ECFCA9F08DA9709315CEA2618,
	DBController_U3CAwakeU3Eb__5_0_m170FC33CA6B9189156D3EC7355F50E5A72C0F0DF,
	U3CAwakeU3Ed__5_MoveNext_m9DEBB6C197CA30AD7C87C04EE9D62BD29CF59696,
	U3CAwakeU3Ed__5_SetStateMachine_mAA66E47DAB6D49BEC13553F63103D53718989F57,
	U3CU3Ec__DisplayClass7_0__ctor_m32C097C8D3927D19E6D0B33A966D3E886E4FC60F,
	U3CU3Ec__DisplayClass8_0__ctor_m27CDD30C1AE8C68FD8CC34BBB44EA23F5C0B56C2,
	U3CU3Ec__DisplayClass9_0__ctor_m1ABB54F977A1A621A10E53AD4B26B57D4112F104,
	U3CU3Ec__DisplayClass10_0__ctor_m2092F92D6819928C01040455240945D7DEFB81E2,
	U3CU3Ec__DisplayClass10_1__ctor_m2A5C21CCBAA4CE4E9ADB43D44D30916C44119E8F,
	U3CU3Ec__DisplayClass10_1_U3CCrearUsuarioU3Eb__2_mEDBF75D8F855D93B54C560DE448861F330E06620,
	U3CU3Ec__DisplayClass11_0__ctor_m06BE024DBF7BA500C7475D6954AFEF13961A7FD0,
	U3CU3Ec__DisplayClass11_0_U3CCambiarContraseUF1aUsuarioU3Eb__0_m0E2296196C5D0C68A233CB0F377104E200572868,
	PlayVideo_stopPlay_m9BE59571EDE6AFDC547C6A72BFC87CC7F71EF1BD,
	PlayVideo__ctor_m12D7A6FF63237382D2F34B45F32BCAC81191FF98,
	PlayVideo__cctor_m75002B1E6552518DF44B60F1AD1D5C3311CFC005,
	Principio_Start_m9C3E0ED7AD0286DA74184754ABE135AB0EAA24FD,
	Principio_Update_m13D0FB1B8B2E7699DA330F2C01D268788420F9C7,
	Principio_OnStartSecuence_mBF596F2F80333266C69EA7F38ABA36AC75DF5377,
	Principio_OnNextMove_m2378376F53320AC9E3B39E9833E2AFBF7D62DAE5,
	Principio__ctor_mB4D0AFE192B15FD63F8CBE35301DEF7D9D40B4EF,
	SnapChange_Cambio_mA55152BF0BDE876B55888AEC013D73330A70F033,
	SnapChange__ctor_m9E15431F272A0917240072ABE32DDDCEA934C0E0,
	User_get_Id_m557E0A6983F4B322084CDC09979DDC1913FF66AE,
	User_set_Id_mE600BFBFBA6B043DCE3EDBC8E2AF4B818C7568F5,
	User_get_Email_m88CF4AB1DAE33A9452CE8A45EC28A620391DE6F8,
	User_set_Email_mF6AA4CDF2DF87668E2F4C47F5490563E1933619A,
	User_get_UserName_mC8FFB977B9C625F062C859415E4BD356B7F9C193,
	User_set_UserName_mB71210FB11CF6055688B95A5BDF211408AB9EA22,
	User_get_UserPass_m028622ED2EBEE91AE8046D9204A0C93CEC70601C,
	User_set_UserPass_mB5F30320C475D01D9789FDE44BB432DC4DB19F4E,
	User__ctor_m1B1BAB8758B1E5F95627EF762681DBE0217D8E0C,
	User__ctor_mA93BAFF15F26978414F373DFC6287FA047561577,
	User_Realms_IRealmObjectBase_get_Accessor_m12CBCE3BD49B16AE3DE40778C45DEBDB71DB1892,
	User_get_Accessor_mCA4F143FC6E898548A360714685D090FDEC942D3,
	User_get_IsManaged_m8AD01061AB39063F5AD1DEF0B828E90366A850D1,
	User_get_IsValid_mD90AB0F8FF7E6BF631CFCF207D2109EBE46F0ADB,
	User_get_IsFrozen_mBA98F094438D6A75D7D7DFFD1B6331A3B20D3C1C,
	User_get_Realm_m2AF41555080EB69C93B6B46F0BE398D4CEF77731,
	User_get_ObjectSchema_m42AB6E21CD662155FAB4E49D95DB59AA04FEDEEB,
	User_get_DynamicApi_m0A89ECF74F20B3AE3E689AD44997D9D6EABF0862,
	User_get_BacklinksCount_m1AE1CAA07F2F28CD49CB9D10119E6038C082B620,
	User_Realms_ISettableManagedAccessor_SetManagedAccessor_m9D62017934A2C00CED0A1895974995D6445C4294,
	User_add__propertyChanged_mE6C054B3EA93CD526DD311F372A61F4F22022290,
	User_remove__propertyChanged_m8E9F5D64F01E2AFB8440B07DCA819F7A9F1A2112,
	User_add_PropertyChanged_mE1B02CDD147F7851DB93D8A47898F1F6DFAEB6FE,
	User_remove_PropertyChanged_mD2B57E0850EE35E3BD6E77A4B82F27059F64AD27,
	User_RaisePropertyChanged_m0AA38E8DAF800A2B858528529D30419B96B394A3,
	User_SubscribeForNotifications_mE4C4005ED50DE3616FE0A5B1B3286750B527E2C3,
	User_UnsubscribeFromNotifications_m0495E735C3B6378BADA976EC03BAF0D1CEAAB9EF,
	User_op_Explicit_mAA1ECB308104CF54010FCBDCF267C91F8F742063,
	User_op_Implicit_m4B230B18890C9F0B95A59425036E9A5D265EDAF2,
	User_GetTypeInfo_mA6A8C2C5837F89043C5DB5E9A2BEEB7C444D4541,
	User_Equals_mDF01803BBA7FD83004E07CF2FFE197964D10599F,
	User_GetHashCode_mF7BDD6792F4AE20B580FE4CD464F06E6994949B5,
	User_ToString_m2783639D63A3A5ED6A18FA475E2105C2EAAD5B7E,
	User__cctor_mDD4F890270946AC9AD9905E28E241BB7CD58528C,
	UserObjectHelper_CopyToRealm_m523B3D8E7977E65D33CD66E78B683AD20FF06085,
	UserObjectHelper_CreateAccessor_mF2DFCFAACF7F8159F67AF1A8EFB4AE3AF57E203A,
	UserObjectHelper_CreateInstance_m2CEC0FB46CAB66C1F7D7DB8505EADC12A7C68D33,
	UserObjectHelper_TryGetPrimaryKeyValue_m9938700ADD3BD9F0F661842E300D5CAE30BCE370,
	UserObjectHelper__ctor_mAA4765896C4EA9C0C941237751D3A51FDA340FDA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UserManagedAccessor_get_Id_m4FDF78B1E78092B3E125D5BA292B22C9C1960480,
	UserManagedAccessor_set_Id_m155B00731406CF29B15F4818687DE86D3B187396,
	UserManagedAccessor_get_Email_m0016AC8779C16A417A226C5212CBD86223607EF8,
	UserManagedAccessor_set_Email_m3F881AE088FCE6507BB7D712DEFFFFF7CCA9C3B4,
	UserManagedAccessor_get_UserName_m740F8387A41A8E721BCE480D80F5F9C68846E060,
	UserManagedAccessor_set_UserName_m7B088714569B80CE4EBD278F172F2047BA0BFA18,
	UserManagedAccessor_get_UserPass_m687D18AEF58374C9303D1345F88372426AD937E2,
	UserManagedAccessor_set_UserPass_mC458D18C8322472812B4DFC102E59B618F415B4A,
	UserManagedAccessor__ctor_m83E59932A712B2EE2BFD48FBFC81B8780959DCF6,
	UserUnmanagedAccessor_get_ObjectSchema_mA3EAB61B5ACE937225C2C6483D3C8AC77D03CC32,
	UserUnmanagedAccessor_get_Id_m3FC9EFFC660EB3B212F25F924B14FC732F558DBC,
	UserUnmanagedAccessor_set_Id_m821FCB6844F6EF34A67B883A017694E3B6D6D3E3,
	UserUnmanagedAccessor_get_Email_m5768CA5CCD06AC94A2E772F1DCA3B8BDC92E8DAB,
	UserUnmanagedAccessor_set_Email_mAF818E3725A01C68F32DF7552D11924CD8BD9AEB,
	UserUnmanagedAccessor_get_UserName_m5A733CC42C49151FEB2C149839E393CA627F4817,
	UserUnmanagedAccessor_set_UserName_m67D3D7A2E5B0BEB1CBF4B51F8E6B80EC6FDD1DA3,
	UserUnmanagedAccessor_get_UserPass_m534E86C59A4B8617E80D40DF02D2E4D5995854D1,
	UserUnmanagedAccessor_set_UserPass_m3124499B5CCF2366F4ED7CCBEE0777EEF0EC41E2,
	UserUnmanagedAccessor__ctor_m454082CE173557E06D236169234D2EC2A5F367C2,
	UserUnmanagedAccessor_GetValue_mE8190C05210C5D2E27BFC629A6FCBC69588201EF,
	UserUnmanagedAccessor_SetValue_m2E8C83A05748B8E33C2AFA8781A90DF2ACADC718,
	UserUnmanagedAccessor_SetValueUnique_m12BA93DBAA708BD9364A4A192E4AEFFE149F3DD8,
	NULL,
	NULL,
	NULL,
	UserSession_get_Id_m5A8577B87A7C08C2519E6360FF5FB00BB7004B5F,
	UserSession_set_Id_m3057DC3BAF8BF17874C8FBEE4BA6F82970C7CF40,
	UserSession_get_LastLoginDate_m17F3AC0D1861FFB1EC85392051329B984AFE88B0,
	UserSession_set_LastLoginDate_m54A0C24A2C42751B580299AF2C3FB558B4C87E83,
	UserSession_get_SessionData_m8446890C4B6F01B6F806115B4261CD1FECA94EA0,
	UserSession_set_SessionData_m8FCC7BAF575446EC54AF905C6763F3E0375D491A,
	UserSession_get_User_mDA7324975F5D05C7045B1D16403ADEE63F8F797F,
	UserSession_set_User_m65A782DC8F9817405E216FDBD6EA141562AF405D,
	UserSession_Realms_IRealmObjectBase_get_Accessor_m6120DCC6D67C3BAF4FFF807A938BA9C26630E478,
	UserSession_get_Accessor_m63953B090B87E08BA1BFE7A44D1E8489D7DBC32F,
	UserSession_get_IsManaged_m11FD78901058CAE201AF02C39887433CE17CA857,
	UserSession_get_IsValid_mBB0774B2D4504CCC1177CA74CDDEE6C38BA0359C,
	UserSession_get_IsFrozen_m83CA809D954798CC7B0CD912932252846BF4FBE7,
	UserSession_get_Realm_m14DC5FB29C73D12D11E6BA8EA3687FB8DCC9445F,
	UserSession_get_ObjectSchema_mD3DF49AD07270FBBD32177E92F1BBA639084F1A2,
	UserSession_get_DynamicApi_mF0EF64999294058BF42D7BC3A81B98B3D498DF3D,
	UserSession_get_BacklinksCount_m260E267AF92CC769D011F20BCA8C1B6F3ED2C6EC,
	UserSession_Realms_ISettableManagedAccessor_SetManagedAccessor_m092E0770BBEC403983296C41E65316C4045EC0CF,
	UserSession_add__propertyChanged_m7B26F46CF4984EEF7452F27585D49B8942ED5218,
	UserSession_remove__propertyChanged_mBC93A1936D8A602849EC8ACC0D53BA462DC1DAB7,
	UserSession_add_PropertyChanged_m0F7648219EBFCD428C9AD7AA8A9559D3CF0942C0,
	UserSession_remove_PropertyChanged_m19DA0F3754D3EFA21831309CD945B4319E7F6DDB,
	UserSession_RaisePropertyChanged_m46270269687EA818A8E551C31698863FAF628078,
	UserSession_SubscribeForNotifications_m5CCE8F8BD5EFC9A4DEA465703C44BCAA24A4C22D,
	UserSession_UnsubscribeFromNotifications_m760D5BD91807904327064F1CF877E3B22524B888,
	UserSession_op_Explicit_mF91738A2C5184678B4FEF6B8E3A3B552C227BE63,
	UserSession_op_Implicit_m1C20940A351D7EDAADA8F85E0CE51E661077F971,
	UserSession_GetTypeInfo_m968F0E52173B072ED9053E380186B1C30FECFE6D,
	UserSession_Equals_m891833BCA3C7336A98CF4B0E9889D06649708BB1,
	UserSession_GetHashCode_m3D1A0624389A32DB13B8CCF26D71AE97E73AE7B7,
	UserSession_ToString_m6F1A5F11605065424F9D16FE5D95A2D47D3EF077,
	UserSession__ctor_m342672E88DD19210377EA9A037DBD93506AE8B1E,
	UserSession__cctor_m681ABE13EA8E4EA6992AC1B393817E9F638F5301,
	UserSessionObjectHelper_CopyToRealm_m45387BA7C5A6F0DCCF437651D6421107A8852189,
	UserSessionObjectHelper_CreateAccessor_m755FEFBCEA322EB3C95E51BE23FE8C809CCF6E01,
	UserSessionObjectHelper_CreateInstance_mC4533DB4D4D8080E8845CC81559F9D35642A3E03,
	UserSessionObjectHelper_TryGetPrimaryKeyValue_m789D4F4B95FD57107244569E8487789E2EB5EF75,
	UserSessionObjectHelper__ctor_m0586222A5538FD81F65F370FFF21B9C61FF42A57,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UserSessionManagedAccessor_get_Id_m02468EB74C25DCFFE85B7D53F724E3BFA2AE63F9,
	UserSessionManagedAccessor_set_Id_mDF5BAB86E262020C31F62C52D253D992A8D224A6,
	UserSessionManagedAccessor_get_LastLoginDate_m65C300B96CA995BDE13A13EEB7B93E74704EFFAA,
	UserSessionManagedAccessor_set_LastLoginDate_m2000A01F3F601FF5897A9B3C70EC468120FD70F5,
	UserSessionManagedAccessor_get_SessionData_m615DE3592BEA60F938138E9E25D423619BB79064,
	UserSessionManagedAccessor_set_SessionData_m534FE5BCBFFF9560318CD2942D207740339254E4,
	UserSessionManagedAccessor_get_User_m63321D619B109BB190495478D4B20D8FAE82B8E9,
	UserSessionManagedAccessor_set_User_m5BE304F726171E1D5DE1ED49C46D9330919CAD4F,
	UserSessionManagedAccessor__ctor_m6691455464325BC2AE2C8F3FD3949CD6AFDF20E5,
	UserSessionUnmanagedAccessor_get_ObjectSchema_m528AEE81454EB2CC43DB2AB29003C2985D0E063F,
	UserSessionUnmanagedAccessor_get_Id_m0AF90DEDC6F5472335B58494F63A5D1644463490,
	UserSessionUnmanagedAccessor_set_Id_m1681EEF72CCF036668E12B563C3A93A4EF9B4C00,
	UserSessionUnmanagedAccessor_get_LastLoginDate_mE675EFAD7CD6803E2BF2ECA215FD8704DE228627,
	UserSessionUnmanagedAccessor_set_LastLoginDate_m59AD4B470C87778D2D77589122492A3ED3BF8BA2,
	UserSessionUnmanagedAccessor_get_SessionData_m29D1C7D150888957797FA22DF52D96CDF774EE1E,
	UserSessionUnmanagedAccessor_set_SessionData_m20C48D76CF2722050B95D0B9AC192777C1741811,
	UserSessionUnmanagedAccessor_get_User_mF0D715502E3DF88F932CB9BA7F158EE1A20538A1,
	UserSessionUnmanagedAccessor_set_User_mD74F504365F14AFD7ECEB2DDFD9EFCAA3B7975DB,
	UserSessionUnmanagedAccessor__ctor_m611EC5D38B86B1B28C299CF9DADB60873A51A4F1,
	UserSessionUnmanagedAccessor_GetValue_m6790CCED6618F6F32B4C61CAC7AFD3BCEA4E5581,
	UserSessionUnmanagedAccessor_SetValue_m0155D5E87C9E1EE5FFB25AD7C00CBA7A244D78EF,
	UserSessionUnmanagedAccessor_SetValueUnique_mA29B60DC46D42104D523EC91102C8B021659B3E1,
	NULL,
	NULL,
	NULL,
	DoNotNotifyAttribute__ctor_m9B4E17379FBAB9411FFDCAC0493791E1D7E7F1FF,
	RealmModuleInitializer_Initialize_m34E16C73A76952675DAEE7A897E90E10AF58CAD9,
};
extern void U3CAwakeU3Ed__5_MoveNext_m9DEBB6C197CA30AD7C87C04EE9D62BD29CF59696_AdjustorThunk (void);
extern void U3CAwakeU3Ed__5_SetStateMachine_mAA66E47DAB6D49BEC13553F63103D53718989F57_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x0600001F, U3CAwakeU3Ed__5_MoveNext_m9DEBB6C197CA30AD7C87C04EE9D62BD29CF59696_AdjustorThunk },
	{ 0x06000020, U3CAwakeU3Ed__5_SetStateMachine_mAA66E47DAB6D49BEC13553F63103D53718989F57_AdjustorThunk },
};
static const int32_t s_InvokerIndices[195] = 
{
	16632,
	10250,
	8401,
	8515,
	8401,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	8515,
	10250,
	10250,
	8515,
	10250,
	10250,
	10250,
	2838,
	3711,
	2838,
	2095,
	3711,
	10250,
	10250,
	10250,
	8515,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	16632,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10250,
	10101,
	8516,
	10100,
	8515,
	10100,
	8515,
	10100,
	8515,
	2416,
	10250,
	10100,
	10100,
	9991,
	9991,
	9991,
	10100,
	10100,
	10022,
	10063,
	1572,
	8515,
	8515,
	8515,
	8515,
	8515,
	10250,
	10250,
	15363,
	15482,
	10100,
	6276,
	10063,
	10100,
	16632,
	2373,
	10100,
	10100,
	2828,
	10250,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	10101,
	8516,
	10100,
	8515,
	10100,
	8515,
	10100,
	8515,
	10250,
	10100,
	10101,
	8516,
	10100,
	8515,
	10100,
	8515,
	10100,
	8515,
	8515,
	7769,
	4605,
	4605,
	0,
	0,
	0,
	10101,
	8516,
	10009,
	8422,
	10100,
	8515,
	10100,
	8515,
	10100,
	10100,
	9991,
	9991,
	9991,
	10100,
	10100,
	10022,
	10063,
	1572,
	8515,
	8515,
	8515,
	8515,
	8515,
	10250,
	10250,
	15363,
	15482,
	10100,
	6276,
	10063,
	10100,
	10250,
	16632,
	2373,
	10100,
	10100,
	2828,
	10250,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	10101,
	8516,
	10009,
	8422,
	10100,
	8515,
	10100,
	8515,
	10250,
	10100,
	10101,
	8516,
	10009,
	8422,
	10100,
	8515,
	10100,
	8515,
	8515,
	7769,
	4605,
	4605,
	0,
	0,
	0,
	10250,
	16632,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	195,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	U3CModuleU3E__cctor_m6AA90A27A1CC5904419E427E5F0AF2419FB211A9, // module initializer,
	NULL,
	NULL,
	NULL,
};

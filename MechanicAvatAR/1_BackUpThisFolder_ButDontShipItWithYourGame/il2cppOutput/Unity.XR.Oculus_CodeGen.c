﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Unity.XR.Oculus.Utils::SetColorScaleAndOffset(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Utils_SetColorScaleAndOffset_mBCF1B16AAE2A720788E10D75CB29AA3A097C48FE (void);
// 0x00000002 Unity.XR.Oculus.SystemHeadset Unity.XR.Oculus.Utils::GetSystemHeadsetType()
extern void Utils_GetSystemHeadsetType_m772BC674D52677149AB8AAE23F51CFA75ADA7FE2 (void);
// 0x00000003 System.Void Unity.XR.Oculus.Utils::PermissionGrantedCallback(System.String)
extern void Utils_PermissionGrantedCallback_mD65CA82802865B79CE79CF496F8129066E6174BB (void);
// 0x00000004 System.Boolean Unity.XR.Oculus.Utils::IsEyeTrackingPermissionGranted()
extern void Utils_IsEyeTrackingPermissionGranted_mFC8EE6843F6ACF2D0731FD7FA11A9CE2A452DE10 (void);
// 0x00000005 System.Boolean Unity.XR.Oculus.Utils::get_useDynamicFoveatedRendering()
extern void Utils_get_useDynamicFoveatedRendering_m41D1D0923B1C7A4C6F488A46A53AEF1A38DE31BE (void);
// 0x00000006 System.Void Unity.XR.Oculus.Utils::set_useDynamicFoveatedRendering(System.Boolean)
extern void Utils_set_useDynamicFoveatedRendering_m8D53693806E771D8BE1104E9E8B0F23958259FD4 (void);
// 0x00000007 System.Int32 Unity.XR.Oculus.Utils::get_foveatedRenderingLevel()
extern void Utils_get_foveatedRenderingLevel_mB977688EBA970B3C69170DDCEDB3F59D63508C27 (void);
// 0x00000008 System.Void Unity.XR.Oculus.Utils::set_foveatedRenderingLevel(System.Int32)
extern void Utils_set_foveatedRenderingLevel_m563B05B055F182F784B8E190059D57C38F668D76 (void);
// 0x00000009 System.Boolean Unity.XR.Oculus.Utils::get_eyeTrackedFoveatedRenderingSupported()
extern void Utils_get_eyeTrackedFoveatedRenderingSupported_mDA6A1FC30088D104AAA32A611092A3394510768C (void);
// 0x0000000A System.Boolean Unity.XR.Oculus.Utils::get_eyeTrackedFoveatedRenderingEnabled()
extern void Utils_get_eyeTrackedFoveatedRenderingEnabled_m3A59F72E842AC8DD55CE3D0C45FB2F00CD22C6FC (void);
// 0x0000000B System.Void Unity.XR.Oculus.Utils::set_eyeTrackedFoveatedRenderingEnabled(System.Boolean)
extern void Utils_set_eyeTrackedFoveatedRenderingEnabled_m4591D257DCFEEC63A57EEEB74A0AAA422C6DFCD3 (void);
// 0x0000000C System.Boolean Unity.XR.Oculus.Utils::SetFoveationLevel(System.Int32)
extern void Utils_SetFoveationLevel_mE53287738F0CEB2FFDCA293687DF3A84F6D3FE6B (void);
// 0x0000000D System.Boolean Unity.XR.Oculus.Utils::EnableDynamicFFR(System.Boolean)
extern void Utils_EnableDynamicFFR_m347FDDB3406996E62E762263E5356325738AE045 (void);
// 0x0000000E System.Int32 Unity.XR.Oculus.Utils::GetFoveationLevel()
extern void Utils_GetFoveationLevel_m4AECAE33089FCE7AAE8ABCA224B63E3DA24A2E42 (void);
// 0x0000000F System.Void Unity.XR.Oculus.InputFocus::add_InputFocusAcquired(System.Action)
extern void InputFocus_add_InputFocusAcquired_m5CD36E7497F9CE17350C428950C12D91436E2CCD (void);
// 0x00000010 System.Void Unity.XR.Oculus.InputFocus::remove_InputFocusAcquired(System.Action)
extern void InputFocus_remove_InputFocusAcquired_m0A73927172E614C9763DF0ADBD73298391177045 (void);
// 0x00000011 System.Void Unity.XR.Oculus.InputFocus::add_InputFocusLost(System.Action)
extern void InputFocus_add_InputFocusLost_m2B3440549F824156867619DA3180FBE76A8AE190 (void);
// 0x00000012 System.Void Unity.XR.Oculus.InputFocus::remove_InputFocusLost(System.Action)
extern void InputFocus_remove_InputFocusLost_m8520D908B784C5042A07F6B8797ABC988EE9C90B (void);
// 0x00000013 System.Boolean Unity.XR.Oculus.InputFocus::get_hasInputFocus()
extern void InputFocus_get_hasInputFocus_m8CB62D564201581EC234AC80FEA79DEF16E2C2F2 (void);
// 0x00000014 System.Void Unity.XR.Oculus.InputFocus::Update()
extern void InputFocus_Update_mF5E717737BF081B1D05D01E9D2AC4D32564694F0 (void);
// 0x00000015 System.Void Unity.XR.Oculus.InputFocus::.ctor()
extern void InputFocus__ctor_m3FDC5A5ECFC5247C91B39C8EE86F768A28CD1F0E (void);
// 0x00000016 System.Boolean Unity.XR.Oculus.Boundary::GetBoundaryConfigured()
extern void Boundary_GetBoundaryConfigured_mFDFAC988DCAB99E021C5438D274A12D5C08553E9 (void);
// 0x00000017 System.Boolean Unity.XR.Oculus.Boundary::GetBoundaryDimensions(Unity.XR.Oculus.Boundary/BoundaryType,UnityEngine.Vector3&)
extern void Boundary_GetBoundaryDimensions_m81665C07EB3DBA51F14A23368958693EC592D2BA (void);
// 0x00000018 System.Boolean Unity.XR.Oculus.Boundary::GetBoundaryVisible()
extern void Boundary_GetBoundaryVisible_m2066498A95DCDB13660F7DCE7A7EAD6DE1C781E1 (void);
// 0x00000019 System.Void Unity.XR.Oculus.Boundary::SetBoundaryVisible(System.Boolean)
extern void Boundary_SetBoundaryVisible_m550F1A4DDE150D503DE9F7F9014E9070176BEB85 (void);
// 0x0000001A System.Void Unity.XR.Oculus.Development::TrySetDeveloperMode(System.Boolean)
extern void Development_TrySetDeveloperMode_m785765B0E2E5285B35FFF96800DFA606D2EF5E00 (void);
// 0x0000001B System.Void Unity.XR.Oculus.Development::OverrideDeveloperModeStart()
extern void Development_OverrideDeveloperModeStart_m7F66F18ADB77A7AF047440A70AD8A49321C0CB04 (void);
// 0x0000001C System.Void Unity.XR.Oculus.Development::OverrideDeveloperModeStop()
extern void Development_OverrideDeveloperModeStop_m3E1041375863ADAB74ADA1D1CB273E1DECD72862 (void);
// 0x0000001D Unity.XR.Oculus.OculusLoader/DeviceSupportedResult Unity.XR.Oculus.OculusLoader::IsDeviceSupported()
extern void OculusLoader_IsDeviceSupported_m1B465FD6B781F4EC026AE1591A09A437D451EDCE (void);
// 0x0000001E UnityEngine.XR.XRDisplaySubsystem Unity.XR.Oculus.OculusLoader::get_displaySubsystem()
extern void OculusLoader_get_displaySubsystem_mBF36D42BABD9D5DA3ECE4F2D25862BF35C29D4E3 (void);
// 0x0000001F UnityEngine.XR.XRInputSubsystem Unity.XR.Oculus.OculusLoader::get_inputSubsystem()
extern void OculusLoader_get_inputSubsystem_m3690AE48575D2196C79757CE5F21C96F1A0963B2 (void);
// 0x00000020 System.Boolean Unity.XR.Oculus.OculusLoader::Initialize()
extern void OculusLoader_Initialize_mDEFC017849247485E672462C30068D016613B08A (void);
// 0x00000021 System.Boolean Unity.XR.Oculus.OculusLoader::Start()
extern void OculusLoader_Start_m0FBB6E2DE04857565E8B5E09165D380E24DBCCBE (void);
// 0x00000022 System.Boolean Unity.XR.Oculus.OculusLoader::Stop()
extern void OculusLoader_Stop_mD8C0412877AD12612ED916895FEB52EA4CADB30F (void);
// 0x00000023 System.Boolean Unity.XR.Oculus.OculusLoader::Deinitialize()
extern void OculusLoader_Deinitialize_mC164D18317DB38A780AD46B671CAD17830ED9930 (void);
// 0x00000024 System.Void Unity.XR.Oculus.OculusLoader::RuntimeLoadOVRPlugin()
extern void OculusLoader_RuntimeLoadOVRPlugin_mDE054BB0D71AC9027DDF4E185387D07B41314397 (void);
// 0x00000025 Unity.XR.Oculus.OculusSettings Unity.XR.Oculus.OculusLoader::GetSettings()
extern void OculusLoader_GetSettings_m2FA61767F36A8CFB03124EB295A79C7A7F0A863B (void);
// 0x00000026 System.Boolean Unity.XR.Oculus.OculusLoader::CheckUnityVersionCompatibility()
extern void OculusLoader_CheckUnityVersionCompatibility_mC211D3EFA35B0E509154B8F2993B08079A5C4413 (void);
// 0x00000027 System.Void Unity.XR.Oculus.OculusLoader::.ctor()
extern void OculusLoader__ctor_m80DAEED59119B8AE0D9F39C2F410CED5AF61007F (void);
// 0x00000028 System.Void Unity.XR.Oculus.OculusLoader::.cctor()
extern void OculusLoader__cctor_m7363F3F6C91405E202624170D8F7323317735396 (void);
// 0x00000029 System.Boolean Unity.XR.Oculus.Performance::TrySetCPULevel(System.Int32)
extern void Performance_TrySetCPULevel_m1A67D6E53EB4896D1525018144EEECDECFDC386A (void);
// 0x0000002A System.Boolean Unity.XR.Oculus.Performance::TrySetGPULevel(System.Int32)
extern void Performance_TrySetGPULevel_m18A9540555E223D81388DFE70AABE1F22A8E6D12 (void);
// 0x0000002B System.Boolean Unity.XR.Oculus.Performance::TryGetAvailableDisplayRefreshRates(System.Single[]&)
extern void Performance_TryGetAvailableDisplayRefreshRates_mDC13DE9810168FBA12B7C035FB47443FE517C112 (void);
// 0x0000002C System.Boolean Unity.XR.Oculus.Performance::TrySetDisplayRefreshRate(System.Single)
extern void Performance_TrySetDisplayRefreshRate_mA986AF6C01393034B30522560FCB9F28319C5FA1 (void);
// 0x0000002D System.Boolean Unity.XR.Oculus.Performance::TryGetDisplayRefreshRate(System.Single&)
extern void Performance_TryGetDisplayRefreshRate_m84EB392DE7AD3E0F3A2AD1C79820B44D663D65B2 (void);
// 0x0000002E System.String Unity.XR.Oculus.Stats::get_PluginVersion()
extern void Stats_get_PluginVersion_mDE3803AC252EA1119F94F44FF7FF40A6C18EDBD9 (void);
// 0x0000002F UnityEngine.IntegratedSubsystem Unity.XR.Oculus.Stats::GetOculusDisplaySubsystem()
extern void Stats_GetOculusDisplaySubsystem_m5A7F2E9A691742F9FCCE65D4482D06B029ABEB0B (void);
// 0x00000030 System.Void Unity.XR.Oculus.Stats::.ctor()
extern void Stats__ctor_m8EE6D4544ED048D96A6679385FFF8EECEA33E08B (void);
// 0x00000031 System.Void Unity.XR.Oculus.Stats::.cctor()
extern void Stats__cctor_mB9016BAA912519CF0C75E95869592AAC5266A495 (void);
// 0x00000032 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_GPUAppTime()
extern void AdaptivePerformance_get_GPUAppTime_m3B2D5EC2B01BE289AB996982E1EB84F953E8D591 (void);
// 0x00000033 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_GPUCompositorTime()
extern void AdaptivePerformance_get_GPUCompositorTime_m6AF1B5D2C388C97CEFBEE4A782DB5B3C4F503416 (void);
// 0x00000034 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_MotionToPhoton()
extern void AdaptivePerformance_get_MotionToPhoton_mA1A3C54D12CC1ABB5B182A4BA6E515530097ADD7 (void);
// 0x00000035 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_RefreshRate()
extern void AdaptivePerformance_get_RefreshRate_m5BB0DDF21E0BEFD8CC1A1EF43A3D53F841B04F7C (void);
// 0x00000036 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_BatteryTemp()
extern void AdaptivePerformance_get_BatteryTemp_mF2742285E38C52383733A82A3BA84D51FF9475E2 (void);
// 0x00000037 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_BatteryLevel()
extern void AdaptivePerformance_get_BatteryLevel_m617DF61FBE36A30C493DEAD5C3CA1C6752A96AB1 (void);
// 0x00000038 System.Boolean Unity.XR.Oculus.Stats/AdaptivePerformance::get_PowerSavingMode()
extern void AdaptivePerformance_get_PowerSavingMode_mC920BDC1E5A06CF98B93F984CC6FCEAACF0DBBDB (void);
// 0x00000039 System.Single Unity.XR.Oculus.Stats/AdaptivePerformance::get_AdaptivePerformanceScale()
extern void AdaptivePerformance_get_AdaptivePerformanceScale_m6529DDA8BCB8F44BD0AF7903263DCE5309EE0007 (void);
// 0x0000003A System.Int32 Unity.XR.Oculus.Stats/AdaptivePerformance::get_CPULevel()
extern void AdaptivePerformance_get_CPULevel_m3DCD68ABD96FA9D9D39C8C3C7261BC999D626492 (void);
// 0x0000003B System.Int32 Unity.XR.Oculus.Stats/AdaptivePerformance::get_GPULevel()
extern void AdaptivePerformance_get_GPULevel_m4BEAF12A43005F89BBFA697C089296D1077C815E (void);
// 0x0000003C System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_AppCPUTime()
extern void PerfMetrics_get_AppCPUTime_m28D85B7DFAD154064CBEF9DBA6099E9D702FD84D (void);
// 0x0000003D System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_AppGPUTime()
extern void PerfMetrics_get_AppGPUTime_m925EFF7AE44FE0166C5A7D2BDD4AA3AEBCBC1DCB (void);
// 0x0000003E System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CompositorCPUTime()
extern void PerfMetrics_get_CompositorCPUTime_m5B7D7E122B01D222A23F4B3E887841707319EC65 (void);
// 0x0000003F System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CompositorGPUTime()
extern void PerfMetrics_get_CompositorGPUTime_mA94CF5C2D35786F1217F491BB54B028FFA997448 (void);
// 0x00000040 System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_GPUUtilization()
extern void PerfMetrics_get_GPUUtilization_m6D6CC3B06B287994F3D8205E56BDEFF6B2CC3C13 (void);
// 0x00000041 System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CPUUtilizationAverage()
extern void PerfMetrics_get_CPUUtilizationAverage_mCA26A4C31FF5C165D157BC4D590F00AAAB428A29 (void);
// 0x00000042 System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CPUUtilizationWorst()
extern void PerfMetrics_get_CPUUtilizationWorst_mF4796A24D39DBD986678B15259609891A80E562F (void);
// 0x00000043 System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_CPUClockFrequency()
extern void PerfMetrics_get_CPUClockFrequency_mB8BB203CB4B17190C6DC88A6E1D3352E1CB4DD47 (void);
// 0x00000044 System.Single Unity.XR.Oculus.Stats/PerfMetrics::get_GPUClockFrequency()
extern void PerfMetrics_get_GPUClockFrequency_m14A0CEA0F7A1977BBF671E1470320B4691362E0F (void);
// 0x00000045 System.Void Unity.XR.Oculus.Stats/PerfMetrics::EnablePerfMetrics(System.Boolean)
extern void PerfMetrics_EnablePerfMetrics_mD249F962C4060F3899ED22233F95F72B2DF596A8 (void);
// 0x00000046 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_AppQueueAheadTime()
extern void AppMetrics_get_AppQueueAheadTime_m58343959F0FE6388CC18CBC734C778006ED2B034 (void);
// 0x00000047 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_AppCPUElapsedTime()
extern void AppMetrics_get_AppCPUElapsedTime_mF167FA15C3C2D950654E4ECBAC5BB95E79315930 (void);
// 0x00000048 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_CompositorDroppedFrames()
extern void AppMetrics_get_CompositorDroppedFrames_mD99E7780BFCF171326C754A31EBD2BC5AFBEE1C1 (void);
// 0x00000049 System.Single Unity.XR.Oculus.Stats/AppMetrics::get_CompositorLatency()
extern void AppMetrics_get_CompositorLatency_mDC2952E3864F2BA367FC80466563B73D3DEF6181 (void);
// 0x0000004A System.Single Unity.XR.Oculus.Stats/AppMetrics::get_CompositorCPUTime()
extern void AppMetrics_get_CompositorCPUTime_m19EBA6AD7F096EA01331C106CA3F776468FB9DF7 (void);
// 0x0000004B System.Single Unity.XR.Oculus.Stats/AppMetrics::get_CPUStartToGPUEnd()
extern void AppMetrics_get_CPUStartToGPUEnd_m656EF39126FA6DA1320C90792A946AAF4E00CE4E (void);
// 0x0000004C System.Single Unity.XR.Oculus.Stats/AppMetrics::get_GPUEndToVsync()
extern void AppMetrics_get_GPUEndToVsync_m25EA0C53CDA56685525D9786BD65E0E41D931F56 (void);
// 0x0000004D System.Void Unity.XR.Oculus.Stats/AppMetrics::EnableAppMetrics(System.Boolean)
extern void AppMetrics_EnableAppMetrics_mF7F5E280BECF6A354040E91DCC4B90D44708F8A6 (void);
// 0x0000004E System.Void Unity.XR.Oculus.NativeMethods::SetColorScale(System.Single,System.Single,System.Single,System.Single)
extern void NativeMethods_SetColorScale_m1C8DF141B1784FA737E3AA5A62AEE0D3F3BC480E (void);
// 0x0000004F System.Void Unity.XR.Oculus.NativeMethods::SetColorOffset(System.Single,System.Single,System.Single,System.Single)
extern void NativeMethods_SetColorOffset_m1F08E1EB6CCD8D77726FA473328C3936C6A5800A (void);
// 0x00000050 System.Boolean Unity.XR.Oculus.NativeMethods::GetIsSupportedDevice()
extern void NativeMethods_GetIsSupportedDevice_mF9F435A347218EF642A3C7C770151F9A4758BBCB (void);
// 0x00000051 System.Boolean Unity.XR.Oculus.NativeMethods::LoadOVRPlugin(System.String)
extern void NativeMethods_LoadOVRPlugin_mAD776BE0CE1A3C7FC318567179EC0D4F83AE3DCD (void);
// 0x00000052 System.Void Unity.XR.Oculus.NativeMethods::UnloadOVRPlugin()
extern void NativeMethods_UnloadOVRPlugin_m76EDAB4F61FDEF92E545D4C3957FB7DA4D170512 (void);
// 0x00000053 System.Void Unity.XR.Oculus.NativeMethods::SetUserDefinedSettings(Unity.XR.Oculus.NativeMethods/UserDefinedSettings)
extern void NativeMethods_SetUserDefinedSettings_m8F63140EA513FAC3AA59EF03F9BA7B8D76BAA70F (void);
// 0x00000054 System.Int32 Unity.XR.Oculus.NativeMethods::SetCPULevel(System.Int32)
extern void NativeMethods_SetCPULevel_m1FA232841B113C4513A510187B153C77C5E24A9D (void);
// 0x00000055 System.Int32 Unity.XR.Oculus.NativeMethods::SetGPULevel(System.Int32)
extern void NativeMethods_SetGPULevel_m76F42DD51A3F0BB1833FF28956D0C496DB16D132 (void);
// 0x00000056 System.Void Unity.XR.Oculus.NativeMethods::GetOVRPVersion(System.Byte[])
extern void NativeMethods_GetOVRPVersion_m9339E90D18E151143160AA02069A532107A63083 (void);
// 0x00000057 System.Void Unity.XR.Oculus.NativeMethods::EnablePerfMetrics(System.Boolean)
extern void NativeMethods_EnablePerfMetrics_m2492CAAFD94EB682B5F5EC9199941BAEBC7C672B (void);
// 0x00000058 System.Void Unity.XR.Oculus.NativeMethods::EnableAppMetrics(System.Boolean)
extern void NativeMethods_EnableAppMetrics_m1E09D2B7FEA3C91CC269F8B2C2A3D1774BDA3587 (void);
// 0x00000059 System.Boolean Unity.XR.Oculus.NativeMethods::SetDeveloperModeStrict(System.Boolean)
extern void NativeMethods_SetDeveloperModeStrict_m02A0EC7138B986A79917C05F817CF75DE804D058 (void);
// 0x0000005A System.Boolean Unity.XR.Oculus.NativeMethods::GetHasInputFocus()
extern void NativeMethods_GetHasInputFocus_m469FAFAF42C3F421244F4F9A09BDA10F79AECF43 (void);
// 0x0000005B System.Boolean Unity.XR.Oculus.NativeMethods::GetBoundaryConfigured()
extern void NativeMethods_GetBoundaryConfigured_mA46B7F80EB13EA0E683C6ECD401EBEBBE16D79F5 (void);
// 0x0000005C System.Boolean Unity.XR.Oculus.NativeMethods::GetBoundaryDimensions(Unity.XR.Oculus.Boundary/BoundaryType,UnityEngine.Vector3&)
extern void NativeMethods_GetBoundaryDimensions_m0BAE0666700C9FA278A3E767F216E46734FFF8F4 (void);
// 0x0000005D System.Boolean Unity.XR.Oculus.NativeMethods::GetBoundaryVisible()
extern void NativeMethods_GetBoundaryVisible_m01035E70C07620D93EFEA287310C2BD1F8922F83 (void);
// 0x0000005E System.Void Unity.XR.Oculus.NativeMethods::SetBoundaryVisible(System.Boolean)
extern void NativeMethods_SetBoundaryVisible_m36857FFC348678CB742169E1D270DA3CAC5C52E1 (void);
// 0x0000005F System.Boolean Unity.XR.Oculus.NativeMethods::GetAppShouldQuit()
extern void NativeMethods_GetAppShouldQuit_m8B8CE94E700636ED76D7E2A38B6B65E5EB77CAE9 (void);
// 0x00000060 System.Boolean Unity.XR.Oculus.NativeMethods::GetDisplayAvailableFrequencies(System.IntPtr,System.Int32&)
extern void NativeMethods_GetDisplayAvailableFrequencies_m5CBDC394EDB259D1FCD2525AD4EDE1C546F1342B (void);
// 0x00000061 System.Boolean Unity.XR.Oculus.NativeMethods::SetDisplayFrequency(System.Single)
extern void NativeMethods_SetDisplayFrequency_mE671959DC912251A8114C9B2B96D3E6C0238427C (void);
// 0x00000062 System.Boolean Unity.XR.Oculus.NativeMethods::GetDisplayFrequency(System.Single&)
extern void NativeMethods_GetDisplayFrequency_mEA796C5C77BB7F2E317D52A9582A7B2D3446DA8E (void);
// 0x00000063 Unity.XR.Oculus.SystemHeadset Unity.XR.Oculus.NativeMethods::GetSystemHeadsetType()
extern void NativeMethods_GetSystemHeadsetType_m02F548DEC426D8D49F5E5997E77CF44DD323EC55 (void);
// 0x00000064 System.Boolean Unity.XR.Oculus.NativeMethods::GetTiledMultiResSupported()
extern void NativeMethods_GetTiledMultiResSupported_m2610D600FFCD16AAD67F58619F7B0DEB4A18BACD (void);
// 0x00000065 System.Void Unity.XR.Oculus.NativeMethods::SetTiledMultiResLevel(System.Int32)
extern void NativeMethods_SetTiledMultiResLevel_m354B87BBBA193EE0F9207AA88B55651E30911445 (void);
// 0x00000066 System.Int32 Unity.XR.Oculus.NativeMethods::GetTiledMultiResLevel()
extern void NativeMethods_GetTiledMultiResLevel_mFF9B2C5A8AD509F19919A2448A65AF2B9F9A81EB (void);
// 0x00000067 System.Void Unity.XR.Oculus.NativeMethods::SetTiledMultiResDynamic(System.Boolean)
extern void NativeMethods_SetTiledMultiResDynamic_m0861F0D2D18A7514EE5A152403A54B473A5551EC (void);
// 0x00000068 System.Boolean Unity.XR.Oculus.NativeMethods::GetEyeTrackedFoveatedRenderingSupported()
extern void NativeMethods_GetEyeTrackedFoveatedRenderingSupported_mF88B7D8DB3BA8951A72A90F09E7977F99940F57A (void);
// 0x00000069 System.Boolean Unity.XR.Oculus.NativeMethods::GetEyeTrackedFoveatedRenderingEnabled()
extern void NativeMethods_GetEyeTrackedFoveatedRenderingEnabled_m953A13B73600221E198C0365C7D8121FA6C254A0 (void);
// 0x0000006A System.Void Unity.XR.Oculus.NativeMethods::SetEyeTrackedFoveatedRenderingEnabled(System.Boolean)
extern void NativeMethods_SetEyeTrackedFoveatedRenderingEnabled_mAAB91AF0E98740A8C113E49FCB099FD46D35A546 (void);
// 0x0000006B System.Boolean Unity.XR.Oculus.NativeMethods::GetShouldRestartSession()
extern void NativeMethods_GetShouldRestartSession_m04E1A4510BA21A584D44942467FC15E5CF6E87E8 (void);
// 0x0000006C System.Void Unity.XR.Oculus.NativeMethods/Internal::SetColorScale(System.Single,System.Single,System.Single,System.Single)
extern void Internal_SetColorScale_mAC28727FE6D4F2F3660EFDF7F50982F802C8CB64 (void);
// 0x0000006D System.Void Unity.XR.Oculus.NativeMethods/Internal::SetColorOffset(System.Single,System.Single,System.Single,System.Single)
extern void Internal_SetColorOffset_m5CDD31B7F5E522310170F5A8ACBE479E62DE0A10 (void);
// 0x0000006E System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetIsSupportedDevice()
extern void Internal_GetIsSupportedDevice_m7530D527AEB2CE8A0F7B6F11238AC8FBF48C7BAF (void);
// 0x0000006F System.Boolean Unity.XR.Oculus.NativeMethods/Internal::LoadOVRPlugin(System.String)
extern void Internal_LoadOVRPlugin_m3F6E66D68CD709C8F8745013B2D924078BF3BA77 (void);
// 0x00000070 System.Void Unity.XR.Oculus.NativeMethods/Internal::UnloadOVRPlugin()
extern void Internal_UnloadOVRPlugin_m69974222D7D37522D4AFB7FF0037EC6E52E7B3AD (void);
// 0x00000071 System.Void Unity.XR.Oculus.NativeMethods/Internal::SetUserDefinedSettings(Unity.XR.Oculus.NativeMethods/UserDefinedSettings)
extern void Internal_SetUserDefinedSettings_mC5F760CAF91842559E576F638FC31777B4BB909E (void);
// 0x00000072 System.Int32 Unity.XR.Oculus.NativeMethods/Internal::SetCPULevel(System.Int32)
extern void Internal_SetCPULevel_mE16D6B1120B73B881A87B3B8E82F91F4B4DC3F00 (void);
// 0x00000073 System.Int32 Unity.XR.Oculus.NativeMethods/Internal::SetGPULevel(System.Int32)
extern void Internal_SetGPULevel_m81B4173CC0AA6E1CF71EE796F4D94732AE239353 (void);
// 0x00000074 System.Void Unity.XR.Oculus.NativeMethods/Internal::GetOVRPVersion(System.Byte[])
extern void Internal_GetOVRPVersion_mB754554431E4660FBF909672068E75CA35CC134D (void);
// 0x00000075 System.Void Unity.XR.Oculus.NativeMethods/Internal::EnablePerfMetrics(System.Boolean)
extern void Internal_EnablePerfMetrics_m90D95D613CA9DFF5A025354FEE9605D82A149EAC (void);
// 0x00000076 System.Void Unity.XR.Oculus.NativeMethods/Internal::EnableAppMetrics(System.Boolean)
extern void Internal_EnableAppMetrics_m37D2C8164A1F6D665A8B917935BC2600ECC2A6E3 (void);
// 0x00000077 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::SetDeveloperModeStrict(System.Boolean)
extern void Internal_SetDeveloperModeStrict_mC178DAA59C11333268F6D2DD6D4BBEB6A28FD5F6 (void);
// 0x00000078 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetAppHasInputFocus()
extern void Internal_GetAppHasInputFocus_mC460DF4EFE848C75E477A0228E62C5955884A0BC (void);
// 0x00000079 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetBoundaryConfigured()
extern void Internal_GetBoundaryConfigured_m77C40D747EA6D194F0029B6597074794875775C9 (void);
// 0x0000007A System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetBoundaryDimensions(Unity.XR.Oculus.Boundary/BoundaryType,UnityEngine.Vector3&)
extern void Internal_GetBoundaryDimensions_m5486CDCCE00FA1D4A3DE95CEBBC81D62797EA586 (void);
// 0x0000007B System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetBoundaryVisible()
extern void Internal_GetBoundaryVisible_m0C5B2309AD11933284B47229B990E760D7CD2904 (void);
// 0x0000007C System.Void Unity.XR.Oculus.NativeMethods/Internal::SetBoundaryVisible(System.Boolean)
extern void Internal_SetBoundaryVisible_m9D7D102E67AFF73DE524274DF5AB06ABC6B725B9 (void);
// 0x0000007D System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetAppShouldQuit()
extern void Internal_GetAppShouldQuit_mFE5F2F0047FE03890E797254F8F04E9D2FC169B2 (void);
// 0x0000007E System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetDisplayAvailableFrequencies(System.IntPtr,System.Int32&)
extern void Internal_GetDisplayAvailableFrequencies_mD87B2B99F82E2A7C8E948F4460DDD94FD1F5DA5A (void);
// 0x0000007F System.Boolean Unity.XR.Oculus.NativeMethods/Internal::SetDisplayFrequency(System.Single)
extern void Internal_SetDisplayFrequency_mF380F22E7D7A1873F5E5D7B59B8C471BE8B40996 (void);
// 0x00000080 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetDisplayFrequency(System.Single&)
extern void Internal_GetDisplayFrequency_m06B03F1EBE989260CC9266D8ECC9E4BB6211C105 (void);
// 0x00000081 Unity.XR.Oculus.SystemHeadset Unity.XR.Oculus.NativeMethods/Internal::GetSystemHeadsetType()
extern void Internal_GetSystemHeadsetType_m86982DAC1289E79F74B3A60F86CF03ACA1B4AA3D (void);
// 0x00000082 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetTiledMultiResSupported()
extern void Internal_GetTiledMultiResSupported_m2638793145460B0A2EFAF01E25EDD8D14B92B037 (void);
// 0x00000083 System.Void Unity.XR.Oculus.NativeMethods/Internal::SetTiledMultiResLevel(System.Int32)
extern void Internal_SetTiledMultiResLevel_m7638F3D237E542D9E4A511F8A3404F839AA267C2 (void);
// 0x00000084 System.Int32 Unity.XR.Oculus.NativeMethods/Internal::GetTiledMultiResLevel()
extern void Internal_GetTiledMultiResLevel_m3119A41A3BE276ED922F58E8C1FD5597A0C717C4 (void);
// 0x00000085 System.Void Unity.XR.Oculus.NativeMethods/Internal::SetTiledMultiResDynamic(System.Boolean)
extern void Internal_SetTiledMultiResDynamic_m65F3A091532C0D2877789AF1D4E01F9B7D522302 (void);
// 0x00000086 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetEyeTrackedFoveatedRenderingSupported()
extern void Internal_GetEyeTrackedFoveatedRenderingSupported_m7F4A3E1F40C329D5C5E40B4DB459BE9218868CD6 (void);
// 0x00000087 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetEyeTrackedFoveatedRenderingEnabled()
extern void Internal_GetEyeTrackedFoveatedRenderingEnabled_mE29550553487E28FAE69C47C5250B62E4CEC63FD (void);
// 0x00000088 System.Void Unity.XR.Oculus.NativeMethods/Internal::SetEyeTrackedFoveatedRenderingEnabled(System.Boolean)
extern void Internal_SetEyeTrackedFoveatedRenderingEnabled_mB19157794B29F35EFF811C439177E96E11E1FF51 (void);
// 0x00000089 System.Boolean Unity.XR.Oculus.NativeMethods/Internal::GetShouldRestartSession()
extern void Internal_GetShouldRestartSession_mBCCB7CCA5389DCD3CB5A48E3B5502BE808AC1342 (void);
// 0x0000008A System.Void Unity.XR.Oculus.OculusRestarter::.cctor()
extern void OculusRestarter__cctor_m7D0916976D23309C17A1BE69E97D1B2C7A140671 (void);
// 0x0000008B System.Void Unity.XR.Oculus.OculusRestarter::ResetCallbacks()
extern void OculusRestarter_ResetCallbacks_mD74C56680A94303936C42A78CD81E1354BB7FE01 (void);
// 0x0000008C System.Boolean Unity.XR.Oculus.OculusRestarter::get_isRunning()
extern void OculusRestarter_get_isRunning_m599C730FB9B795105BD3815E45E6F134479EA365 (void);
// 0x0000008D System.Single Unity.XR.Oculus.OculusRestarter::get_TimeBetweenRestartAttempts()
extern void OculusRestarter_get_TimeBetweenRestartAttempts_m695390044FD2215448CAC97FA36B06D29DF4CFB0 (void);
// 0x0000008E System.Void Unity.XR.Oculus.OculusRestarter::set_TimeBetweenRestartAttempts(System.Single)
extern void OculusRestarter_set_TimeBetweenRestartAttempts_m2CAAFD6EBED5672288BB1632A2D223011669BB02 (void);
// 0x0000008F System.Int32 Unity.XR.Oculus.OculusRestarter::get_PauseAndRestartAttempts()
extern void OculusRestarter_get_PauseAndRestartAttempts_m708D7BAF2DD2F503A04FDFB22BBD6F7A2D02FEBF (void);
// 0x00000090 Unity.XR.Oculus.OculusRestarter Unity.XR.Oculus.OculusRestarter::get_Instance()
extern void OculusRestarter_get_Instance_m22FDEC25C6918D95A9D0900440A9120B40972CAA (void);
// 0x00000091 System.Void Unity.XR.Oculus.OculusRestarter::PauseAndRestart()
extern void OculusRestarter_PauseAndRestart_m334143EB5A9044C39B682BBA52B4A5289F0A8794 (void);
// 0x00000092 System.Collections.IEnumerator Unity.XR.Oculus.OculusRestarter::PauseAndRestartCoroutine(System.Single)
extern void OculusRestarter_PauseAndRestartCoroutine_m27BFEF6F9B0BDC391E58682CBFC448B2B6D25B6C (void);
// 0x00000093 System.Collections.IEnumerator Unity.XR.Oculus.OculusRestarter::RestartCoroutine(System.Boolean)
extern void OculusRestarter_RestartCoroutine_m1700733D83EF875AC1FB3800A644492B0D326732 (void);
// 0x00000094 System.Void Unity.XR.Oculus.OculusRestarter::.ctor()
extern void OculusRestarter__ctor_m15272859C8EE853B874F169EA43ED3484FEECE25 (void);
// 0x00000095 System.Void Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::.ctor(System.Int32)
extern void U3CPauseAndRestartCoroutineU3Ed__22__ctor_m27A9F02C46B3238DBAB66985ADBFFCC45FE55EA5 (void);
// 0x00000096 System.Void Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::System.IDisposable.Dispose()
extern void U3CPauseAndRestartCoroutineU3Ed__22_System_IDisposable_Dispose_m3F165058000C6AC7BB0AD2C2232553F99B5C2974 (void);
// 0x00000097 System.Boolean Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::MoveNext()
extern void U3CPauseAndRestartCoroutineU3Ed__22_MoveNext_m10B3943D1C1524ECC03BE075BF4B2AB75890D153 (void);
// 0x00000098 System.Void Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::<>m__Finally1()
extern void U3CPauseAndRestartCoroutineU3Ed__22_U3CU3Em__Finally1_m9C251557284CEF82BFC5B8E3959EA8F44D64E2AC (void);
// 0x00000099 System.Object Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94B64528A36F820EDC3850DF8454AA1B35B97A37 (void);
// 0x0000009A System.Void Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::System.Collections.IEnumerator.Reset()
extern void U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_IEnumerator_Reset_m75F176C3F999B4F432B0B14EC5A20E99EE8747DE (void);
// 0x0000009B System.Object Unity.XR.Oculus.OculusRestarter/<PauseAndRestartCoroutine>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_IEnumerator_get_Current_m31FB59029F60BB8DDE4B5437E2B8D73AC55D2B8A (void);
// 0x0000009C System.Void Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::.ctor(System.Int32)
extern void U3CRestartCoroutineU3Ed__23__ctor_mB8A5A22DE3789D9284962619E7C3EEE83DD430E6 (void);
// 0x0000009D System.Void Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::System.IDisposable.Dispose()
extern void U3CRestartCoroutineU3Ed__23_System_IDisposable_Dispose_mCEF1C34CE2A4B91A0F2117134510776A4D3632FB (void);
// 0x0000009E System.Boolean Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::MoveNext()
extern void U3CRestartCoroutineU3Ed__23_MoveNext_m099D9A712E54D633CA35C51AE63650AAB59D390A (void);
// 0x0000009F System.Void Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::<>m__Finally1()
extern void U3CRestartCoroutineU3Ed__23_U3CU3Em__Finally1_m46F9F866F380F9ACB4EC8CAA89335B152B8343B2 (void);
// 0x000000A0 System.Object Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestartCoroutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5728FD3D1B331737D2D00F9192B068C0BD823962 (void);
// 0x000000A1 System.Void Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::System.Collections.IEnumerator.Reset()
extern void U3CRestartCoroutineU3Ed__23_System_Collections_IEnumerator_Reset_mD0FB204A82BF5FEA25BB4745679BA400DA54AAA2 (void);
// 0x000000A2 System.Object Unity.XR.Oculus.OculusRestarter/<RestartCoroutine>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CRestartCoroutineU3Ed__23_System_Collections_IEnumerator_get_Current_m1A36F2FC3B732CE389BC67D86EF8541D4196B41C (void);
// 0x000000A3 System.Void Unity.XR.Oculus.OculusSession::Update()
extern void OculusSession_Update_mC3110176B22CC4095723E61ECF87A0D81E82D2B7 (void);
// 0x000000A4 System.UInt16 Unity.XR.Oculus.OculusSettings::GetStereoRenderingMode()
extern void OculusSettings_GetStereoRenderingMode_mE9C6ABC56B9EDEB3BE5599B091BB6699429BB6BD (void);
// 0x000000A5 System.Void Unity.XR.Oculus.OculusSettings::Awake()
extern void OculusSettings_Awake_m6206BE150DEB602C65EEF7C255DE3BA17FB1452E (void);
// 0x000000A6 System.Void Unity.XR.Oculus.OculusSettings::.ctor()
extern void OculusSettings__ctor_m2A441A38D66B2B4538A2CE744BCFD3283910F324 (void);
// 0x000000A7 System.Void Unity.XR.Oculus.OculusUsages::.cctor()
extern void OculusUsages__cctor_m3BC2BDB6198C21AF208C5DD0C8A59C5A2D8E7129 (void);
// 0x000000A8 System.Void Unity.XR.Oculus.RegisterUpdateCallback::Initialize()
extern void RegisterUpdateCallback_Initialize_m041B05AB55DFBFA8F93BF453AB89A68C5DD2FFE6 (void);
// 0x000000A9 System.Void Unity.XR.Oculus.RegisterUpdateCallback::Deinitialize()
extern void RegisterUpdateCallback_Deinitialize_m8AE7F6714C9AA27ECAF8DF1C48B771DABBC292D4 (void);
// 0x000000AA System.Void Unity.XR.Oculus.RegisterUpdateCallback::Update()
extern void RegisterUpdateCallback_Update_mD7B551FC22C28DA0808C540C95B2B401497D84CF (void);
static Il2CppMethodPointer s_methodPointers[170] = 
{
	Utils_SetColorScaleAndOffset_mBCF1B16AAE2A720788E10D75CB29AA3A097C48FE,
	Utils_GetSystemHeadsetType_m772BC674D52677149AB8AAE23F51CFA75ADA7FE2,
	Utils_PermissionGrantedCallback_mD65CA82802865B79CE79CF496F8129066E6174BB,
	Utils_IsEyeTrackingPermissionGranted_mFC8EE6843F6ACF2D0731FD7FA11A9CE2A452DE10,
	Utils_get_useDynamicFoveatedRendering_m41D1D0923B1C7A4C6F488A46A53AEF1A38DE31BE,
	Utils_set_useDynamicFoveatedRendering_m8D53693806E771D8BE1104E9E8B0F23958259FD4,
	Utils_get_foveatedRenderingLevel_mB977688EBA970B3C69170DDCEDB3F59D63508C27,
	Utils_set_foveatedRenderingLevel_m563B05B055F182F784B8E190059D57C38F668D76,
	Utils_get_eyeTrackedFoveatedRenderingSupported_mDA6A1FC30088D104AAA32A611092A3394510768C,
	Utils_get_eyeTrackedFoveatedRenderingEnabled_m3A59F72E842AC8DD55CE3D0C45FB2F00CD22C6FC,
	Utils_set_eyeTrackedFoveatedRenderingEnabled_m4591D257DCFEEC63A57EEEB74A0AAA422C6DFCD3,
	Utils_SetFoveationLevel_mE53287738F0CEB2FFDCA293687DF3A84F6D3FE6B,
	Utils_EnableDynamicFFR_m347FDDB3406996E62E762263E5356325738AE045,
	Utils_GetFoveationLevel_m4AECAE33089FCE7AAE8ABCA224B63E3DA24A2E42,
	InputFocus_add_InputFocusAcquired_m5CD36E7497F9CE17350C428950C12D91436E2CCD,
	InputFocus_remove_InputFocusAcquired_m0A73927172E614C9763DF0ADBD73298391177045,
	InputFocus_add_InputFocusLost_m2B3440549F824156867619DA3180FBE76A8AE190,
	InputFocus_remove_InputFocusLost_m8520D908B784C5042A07F6B8797ABC988EE9C90B,
	InputFocus_get_hasInputFocus_m8CB62D564201581EC234AC80FEA79DEF16E2C2F2,
	InputFocus_Update_mF5E717737BF081B1D05D01E9D2AC4D32564694F0,
	InputFocus__ctor_m3FDC5A5ECFC5247C91B39C8EE86F768A28CD1F0E,
	Boundary_GetBoundaryConfigured_mFDFAC988DCAB99E021C5438D274A12D5C08553E9,
	Boundary_GetBoundaryDimensions_m81665C07EB3DBA51F14A23368958693EC592D2BA,
	Boundary_GetBoundaryVisible_m2066498A95DCDB13660F7DCE7A7EAD6DE1C781E1,
	Boundary_SetBoundaryVisible_m550F1A4DDE150D503DE9F7F9014E9070176BEB85,
	Development_TrySetDeveloperMode_m785765B0E2E5285B35FFF96800DFA606D2EF5E00,
	Development_OverrideDeveloperModeStart_m7F66F18ADB77A7AF047440A70AD8A49321C0CB04,
	Development_OverrideDeveloperModeStop_m3E1041375863ADAB74ADA1D1CB273E1DECD72862,
	OculusLoader_IsDeviceSupported_m1B465FD6B781F4EC026AE1591A09A437D451EDCE,
	OculusLoader_get_displaySubsystem_mBF36D42BABD9D5DA3ECE4F2D25862BF35C29D4E3,
	OculusLoader_get_inputSubsystem_m3690AE48575D2196C79757CE5F21C96F1A0963B2,
	OculusLoader_Initialize_mDEFC017849247485E672462C30068D016613B08A,
	OculusLoader_Start_m0FBB6E2DE04857565E8B5E09165D380E24DBCCBE,
	OculusLoader_Stop_mD8C0412877AD12612ED916895FEB52EA4CADB30F,
	OculusLoader_Deinitialize_mC164D18317DB38A780AD46B671CAD17830ED9930,
	OculusLoader_RuntimeLoadOVRPlugin_mDE054BB0D71AC9027DDF4E185387D07B41314397,
	OculusLoader_GetSettings_m2FA61767F36A8CFB03124EB295A79C7A7F0A863B,
	OculusLoader_CheckUnityVersionCompatibility_mC211D3EFA35B0E509154B8F2993B08079A5C4413,
	OculusLoader__ctor_m80DAEED59119B8AE0D9F39C2F410CED5AF61007F,
	OculusLoader__cctor_m7363F3F6C91405E202624170D8F7323317735396,
	Performance_TrySetCPULevel_m1A67D6E53EB4896D1525018144EEECDECFDC386A,
	Performance_TrySetGPULevel_m18A9540555E223D81388DFE70AABE1F22A8E6D12,
	Performance_TryGetAvailableDisplayRefreshRates_mDC13DE9810168FBA12B7C035FB47443FE517C112,
	Performance_TrySetDisplayRefreshRate_mA986AF6C01393034B30522560FCB9F28319C5FA1,
	Performance_TryGetDisplayRefreshRate_m84EB392DE7AD3E0F3A2AD1C79820B44D663D65B2,
	Stats_get_PluginVersion_mDE3803AC252EA1119F94F44FF7FF40A6C18EDBD9,
	Stats_GetOculusDisplaySubsystem_m5A7F2E9A691742F9FCCE65D4482D06B029ABEB0B,
	Stats__ctor_m8EE6D4544ED048D96A6679385FFF8EECEA33E08B,
	Stats__cctor_mB9016BAA912519CF0C75E95869592AAC5266A495,
	AdaptivePerformance_get_GPUAppTime_m3B2D5EC2B01BE289AB996982E1EB84F953E8D591,
	AdaptivePerformance_get_GPUCompositorTime_m6AF1B5D2C388C97CEFBEE4A782DB5B3C4F503416,
	AdaptivePerformance_get_MotionToPhoton_mA1A3C54D12CC1ABB5B182A4BA6E515530097ADD7,
	AdaptivePerformance_get_RefreshRate_m5BB0DDF21E0BEFD8CC1A1EF43A3D53F841B04F7C,
	AdaptivePerformance_get_BatteryTemp_mF2742285E38C52383733A82A3BA84D51FF9475E2,
	AdaptivePerformance_get_BatteryLevel_m617DF61FBE36A30C493DEAD5C3CA1C6752A96AB1,
	AdaptivePerformance_get_PowerSavingMode_mC920BDC1E5A06CF98B93F984CC6FCEAACF0DBBDB,
	AdaptivePerformance_get_AdaptivePerformanceScale_m6529DDA8BCB8F44BD0AF7903263DCE5309EE0007,
	AdaptivePerformance_get_CPULevel_m3DCD68ABD96FA9D9D39C8C3C7261BC999D626492,
	AdaptivePerformance_get_GPULevel_m4BEAF12A43005F89BBFA697C089296D1077C815E,
	PerfMetrics_get_AppCPUTime_m28D85B7DFAD154064CBEF9DBA6099E9D702FD84D,
	PerfMetrics_get_AppGPUTime_m925EFF7AE44FE0166C5A7D2BDD4AA3AEBCBC1DCB,
	PerfMetrics_get_CompositorCPUTime_m5B7D7E122B01D222A23F4B3E887841707319EC65,
	PerfMetrics_get_CompositorGPUTime_mA94CF5C2D35786F1217F491BB54B028FFA997448,
	PerfMetrics_get_GPUUtilization_m6D6CC3B06B287994F3D8205E56BDEFF6B2CC3C13,
	PerfMetrics_get_CPUUtilizationAverage_mCA26A4C31FF5C165D157BC4D590F00AAAB428A29,
	PerfMetrics_get_CPUUtilizationWorst_mF4796A24D39DBD986678B15259609891A80E562F,
	PerfMetrics_get_CPUClockFrequency_mB8BB203CB4B17190C6DC88A6E1D3352E1CB4DD47,
	PerfMetrics_get_GPUClockFrequency_m14A0CEA0F7A1977BBF671E1470320B4691362E0F,
	PerfMetrics_EnablePerfMetrics_mD249F962C4060F3899ED22233F95F72B2DF596A8,
	AppMetrics_get_AppQueueAheadTime_m58343959F0FE6388CC18CBC734C778006ED2B034,
	AppMetrics_get_AppCPUElapsedTime_mF167FA15C3C2D950654E4ECBAC5BB95E79315930,
	AppMetrics_get_CompositorDroppedFrames_mD99E7780BFCF171326C754A31EBD2BC5AFBEE1C1,
	AppMetrics_get_CompositorLatency_mDC2952E3864F2BA367FC80466563B73D3DEF6181,
	AppMetrics_get_CompositorCPUTime_m19EBA6AD7F096EA01331C106CA3F776468FB9DF7,
	AppMetrics_get_CPUStartToGPUEnd_m656EF39126FA6DA1320C90792A946AAF4E00CE4E,
	AppMetrics_get_GPUEndToVsync_m25EA0C53CDA56685525D9786BD65E0E41D931F56,
	AppMetrics_EnableAppMetrics_mF7F5E280BECF6A354040E91DCC4B90D44708F8A6,
	NativeMethods_SetColorScale_m1C8DF141B1784FA737E3AA5A62AEE0D3F3BC480E,
	NativeMethods_SetColorOffset_m1F08E1EB6CCD8D77726FA473328C3936C6A5800A,
	NativeMethods_GetIsSupportedDevice_mF9F435A347218EF642A3C7C770151F9A4758BBCB,
	NativeMethods_LoadOVRPlugin_mAD776BE0CE1A3C7FC318567179EC0D4F83AE3DCD,
	NativeMethods_UnloadOVRPlugin_m76EDAB4F61FDEF92E545D4C3957FB7DA4D170512,
	NativeMethods_SetUserDefinedSettings_m8F63140EA513FAC3AA59EF03F9BA7B8D76BAA70F,
	NativeMethods_SetCPULevel_m1FA232841B113C4513A510187B153C77C5E24A9D,
	NativeMethods_SetGPULevel_m76F42DD51A3F0BB1833FF28956D0C496DB16D132,
	NativeMethods_GetOVRPVersion_m9339E90D18E151143160AA02069A532107A63083,
	NativeMethods_EnablePerfMetrics_m2492CAAFD94EB682B5F5EC9199941BAEBC7C672B,
	NativeMethods_EnableAppMetrics_m1E09D2B7FEA3C91CC269F8B2C2A3D1774BDA3587,
	NativeMethods_SetDeveloperModeStrict_m02A0EC7138B986A79917C05F817CF75DE804D058,
	NativeMethods_GetHasInputFocus_m469FAFAF42C3F421244F4F9A09BDA10F79AECF43,
	NativeMethods_GetBoundaryConfigured_mA46B7F80EB13EA0E683C6ECD401EBEBBE16D79F5,
	NativeMethods_GetBoundaryDimensions_m0BAE0666700C9FA278A3E767F216E46734FFF8F4,
	NativeMethods_GetBoundaryVisible_m01035E70C07620D93EFEA287310C2BD1F8922F83,
	NativeMethods_SetBoundaryVisible_m36857FFC348678CB742169E1D270DA3CAC5C52E1,
	NativeMethods_GetAppShouldQuit_m8B8CE94E700636ED76D7E2A38B6B65E5EB77CAE9,
	NativeMethods_GetDisplayAvailableFrequencies_m5CBDC394EDB259D1FCD2525AD4EDE1C546F1342B,
	NativeMethods_SetDisplayFrequency_mE671959DC912251A8114C9B2B96D3E6C0238427C,
	NativeMethods_GetDisplayFrequency_mEA796C5C77BB7F2E317D52A9582A7B2D3446DA8E,
	NativeMethods_GetSystemHeadsetType_m02F548DEC426D8D49F5E5997E77CF44DD323EC55,
	NativeMethods_GetTiledMultiResSupported_m2610D600FFCD16AAD67F58619F7B0DEB4A18BACD,
	NativeMethods_SetTiledMultiResLevel_m354B87BBBA193EE0F9207AA88B55651E30911445,
	NativeMethods_GetTiledMultiResLevel_mFF9B2C5A8AD509F19919A2448A65AF2B9F9A81EB,
	NativeMethods_SetTiledMultiResDynamic_m0861F0D2D18A7514EE5A152403A54B473A5551EC,
	NativeMethods_GetEyeTrackedFoveatedRenderingSupported_mF88B7D8DB3BA8951A72A90F09E7977F99940F57A,
	NativeMethods_GetEyeTrackedFoveatedRenderingEnabled_m953A13B73600221E198C0365C7D8121FA6C254A0,
	NativeMethods_SetEyeTrackedFoveatedRenderingEnabled_mAAB91AF0E98740A8C113E49FCB099FD46D35A546,
	NativeMethods_GetShouldRestartSession_m04E1A4510BA21A584D44942467FC15E5CF6E87E8,
	Internal_SetColorScale_mAC28727FE6D4F2F3660EFDF7F50982F802C8CB64,
	Internal_SetColorOffset_m5CDD31B7F5E522310170F5A8ACBE479E62DE0A10,
	Internal_GetIsSupportedDevice_m7530D527AEB2CE8A0F7B6F11238AC8FBF48C7BAF,
	Internal_LoadOVRPlugin_m3F6E66D68CD709C8F8745013B2D924078BF3BA77,
	Internal_UnloadOVRPlugin_m69974222D7D37522D4AFB7FF0037EC6E52E7B3AD,
	Internal_SetUserDefinedSettings_mC5F760CAF91842559E576F638FC31777B4BB909E,
	Internal_SetCPULevel_mE16D6B1120B73B881A87B3B8E82F91F4B4DC3F00,
	Internal_SetGPULevel_m81B4173CC0AA6E1CF71EE796F4D94732AE239353,
	Internal_GetOVRPVersion_mB754554431E4660FBF909672068E75CA35CC134D,
	Internal_EnablePerfMetrics_m90D95D613CA9DFF5A025354FEE9605D82A149EAC,
	Internal_EnableAppMetrics_m37D2C8164A1F6D665A8B917935BC2600ECC2A6E3,
	Internal_SetDeveloperModeStrict_mC178DAA59C11333268F6D2DD6D4BBEB6A28FD5F6,
	Internal_GetAppHasInputFocus_mC460DF4EFE848C75E477A0228E62C5955884A0BC,
	Internal_GetBoundaryConfigured_m77C40D747EA6D194F0029B6597074794875775C9,
	Internal_GetBoundaryDimensions_m5486CDCCE00FA1D4A3DE95CEBBC81D62797EA586,
	Internal_GetBoundaryVisible_m0C5B2309AD11933284B47229B990E760D7CD2904,
	Internal_SetBoundaryVisible_m9D7D102E67AFF73DE524274DF5AB06ABC6B725B9,
	Internal_GetAppShouldQuit_mFE5F2F0047FE03890E797254F8F04E9D2FC169B2,
	Internal_GetDisplayAvailableFrequencies_mD87B2B99F82E2A7C8E948F4460DDD94FD1F5DA5A,
	Internal_SetDisplayFrequency_mF380F22E7D7A1873F5E5D7B59B8C471BE8B40996,
	Internal_GetDisplayFrequency_m06B03F1EBE989260CC9266D8ECC9E4BB6211C105,
	Internal_GetSystemHeadsetType_m86982DAC1289E79F74B3A60F86CF03ACA1B4AA3D,
	Internal_GetTiledMultiResSupported_m2638793145460B0A2EFAF01E25EDD8D14B92B037,
	Internal_SetTiledMultiResLevel_m7638F3D237E542D9E4A511F8A3404F839AA267C2,
	Internal_GetTiledMultiResLevel_m3119A41A3BE276ED922F58E8C1FD5597A0C717C4,
	Internal_SetTiledMultiResDynamic_m65F3A091532C0D2877789AF1D4E01F9B7D522302,
	Internal_GetEyeTrackedFoveatedRenderingSupported_m7F4A3E1F40C329D5C5E40B4DB459BE9218868CD6,
	Internal_GetEyeTrackedFoveatedRenderingEnabled_mE29550553487E28FAE69C47C5250B62E4CEC63FD,
	Internal_SetEyeTrackedFoveatedRenderingEnabled_mB19157794B29F35EFF811C439177E96E11E1FF51,
	Internal_GetShouldRestartSession_mBCCB7CCA5389DCD3CB5A48E3B5502BE808AC1342,
	OculusRestarter__cctor_m7D0916976D23309C17A1BE69E97D1B2C7A140671,
	OculusRestarter_ResetCallbacks_mD74C56680A94303936C42A78CD81E1354BB7FE01,
	OculusRestarter_get_isRunning_m599C730FB9B795105BD3815E45E6F134479EA365,
	OculusRestarter_get_TimeBetweenRestartAttempts_m695390044FD2215448CAC97FA36B06D29DF4CFB0,
	OculusRestarter_set_TimeBetweenRestartAttempts_m2CAAFD6EBED5672288BB1632A2D223011669BB02,
	OculusRestarter_get_PauseAndRestartAttempts_m708D7BAF2DD2F503A04FDFB22BBD6F7A2D02FEBF,
	OculusRestarter_get_Instance_m22FDEC25C6918D95A9D0900440A9120B40972CAA,
	OculusRestarter_PauseAndRestart_m334143EB5A9044C39B682BBA52B4A5289F0A8794,
	OculusRestarter_PauseAndRestartCoroutine_m27BFEF6F9B0BDC391E58682CBFC448B2B6D25B6C,
	OculusRestarter_RestartCoroutine_m1700733D83EF875AC1FB3800A644492B0D326732,
	OculusRestarter__ctor_m15272859C8EE853B874F169EA43ED3484FEECE25,
	U3CPauseAndRestartCoroutineU3Ed__22__ctor_m27A9F02C46B3238DBAB66985ADBFFCC45FE55EA5,
	U3CPauseAndRestartCoroutineU3Ed__22_System_IDisposable_Dispose_m3F165058000C6AC7BB0AD2C2232553F99B5C2974,
	U3CPauseAndRestartCoroutineU3Ed__22_MoveNext_m10B3943D1C1524ECC03BE075BF4B2AB75890D153,
	U3CPauseAndRestartCoroutineU3Ed__22_U3CU3Em__Finally1_m9C251557284CEF82BFC5B8E3959EA8F44D64E2AC,
	U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94B64528A36F820EDC3850DF8454AA1B35B97A37,
	U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_IEnumerator_Reset_m75F176C3F999B4F432B0B14EC5A20E99EE8747DE,
	U3CPauseAndRestartCoroutineU3Ed__22_System_Collections_IEnumerator_get_Current_m31FB59029F60BB8DDE4B5437E2B8D73AC55D2B8A,
	U3CRestartCoroutineU3Ed__23__ctor_mB8A5A22DE3789D9284962619E7C3EEE83DD430E6,
	U3CRestartCoroutineU3Ed__23_System_IDisposable_Dispose_mCEF1C34CE2A4B91A0F2117134510776A4D3632FB,
	U3CRestartCoroutineU3Ed__23_MoveNext_m099D9A712E54D633CA35C51AE63650AAB59D390A,
	U3CRestartCoroutineU3Ed__23_U3CU3Em__Finally1_m46F9F866F380F9ACB4EC8CAA89335B152B8343B2,
	U3CRestartCoroutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5728FD3D1B331737D2D00F9192B068C0BD823962,
	U3CRestartCoroutineU3Ed__23_System_Collections_IEnumerator_Reset_mD0FB204A82BF5FEA25BB4745679BA400DA54AAA2,
	U3CRestartCoroutineU3Ed__23_System_Collections_IEnumerator_get_Current_m1A36F2FC3B732CE389BC67D86EF8541D4196B41C,
	OculusSession_Update_mC3110176B22CC4095723E61ECF87A0D81E82D2B7,
	OculusSettings_GetStereoRenderingMode_mE9C6ABC56B9EDEB3BE5599B091BB6699429BB6BD,
	OculusSettings_Awake_m6206BE150DEB602C65EEF7C255DE3BA17FB1452E,
	OculusSettings__ctor_m2A441A38D66B2B4538A2CE744BCFD3283910F324,
	OculusUsages__cctor_m3BC2BDB6198C21AF208C5DD0C8A59C5A2D8E7129,
	RegisterUpdateCallback_Initialize_m041B05AB55DFBFA8F93BF453AB89A68C5DD2FFE6,
	RegisterUpdateCallback_Deinitialize_m8AE7F6714C9AA27ECAF8DF1C48B771DABBC292D4,
	RegisterUpdateCallback_Update_mD7B551FC22C28DA0808C540C95B2B401497D84CF,
};
static const int32_t s_InvokerIndices[170] = 
{
	10310,
	12407,
	11599,
	12388,
	12388,
	11591,
	12407,
	11595,
	12388,
	12388,
	11591,
	11094,
	11090,
	12407,
	11599,
	11599,
	11599,
	11599,
	12388,
	12456,
	7071,
	12388,
	9547,
	12388,
	11591,
	11591,
	12456,
	12456,
	12407,
	6937,
	6937,
	6840,
	6840,
	6840,
	6840,
	12456,
	6937,
	6840,
	7071,
	12456,
	11094,
	11094,
	11088,
	11102,
	11088,
	12415,
	12415,
	7071,
	12456,
	12437,
	12437,
	12437,
	12437,
	12437,
	12437,
	12388,
	12437,
	12407,
	12407,
	12437,
	12437,
	12437,
	12437,
	12437,
	12437,
	12437,
	12437,
	12437,
	11591,
	12437,
	12437,
	12437,
	12437,
	12437,
	12437,
	12437,
	11591,
	8503,
	8503,
	12388,
	11097,
	12456,
	11611,
	11212,
	11212,
	11599,
	11591,
	11591,
	11090,
	12388,
	12388,
	9547,
	12388,
	11591,
	12388,
	9559,
	11102,
	11088,
	12407,
	12388,
	11595,
	12407,
	11591,
	12388,
	12388,
	11591,
	12388,
	8503,
	8503,
	12388,
	11097,
	12456,
	11611,
	11212,
	11212,
	11599,
	11591,
	11591,
	11090,
	12388,
	12388,
	9547,
	12388,
	11591,
	12388,
	9559,
	11102,
	11088,
	12407,
	12388,
	11595,
	12407,
	11591,
	12388,
	12388,
	11591,
	12388,
	12456,
	7071,
	6840,
	12437,
	11605,
	12407,
	12415,
	7071,
	5066,
	5050,
	7071,
	5627,
	7071,
	6840,
	7071,
	6937,
	7071,
	6937,
	5627,
	7071,
	6840,
	7071,
	6937,
	7071,
	6937,
	12456,
	7052,
	7071,
	7071,
	12456,
	12456,
	12456,
	12456,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_XR_Oculus_CodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_Oculus_CodeGenModule = 
{
	"Unity.XR.Oculus.dll",
	170,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

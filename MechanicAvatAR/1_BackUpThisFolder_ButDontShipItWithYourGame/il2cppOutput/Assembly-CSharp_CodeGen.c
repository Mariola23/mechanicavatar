﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AutoPlay::Start()
extern void AutoPlay_Start_mB70D4770E116E6CF9F59384ACADD89A37DC62D20 (void);
// 0x00000002 System.Void AutoPlay::Update()
extern void AutoPlay_Update_mDFBBCA2D848E04E091E09E130B0443F0FC9B4428 (void);
// 0x00000003 System.Void AutoPlay::.ctor()
extern void AutoPlay__ctor_m0048F0EBF8281CE23AB62A575A8C7D2F87A3F381 (void);
// 0x00000004 System.Void Choque::Start()
extern void Choque_Start_m47A0EBF9644517E33267B617CC7244C1B3CAA9C4 (void);
// 0x00000005 System.Void Choque::OnCollisionEnter(UnityEngine.Collision)
extern void Choque_OnCollisionEnter_m49855E353909337CB23C00B96134ED2CC12ED65A (void);
// 0x00000006 System.Void Choque::.ctor()
extern void Choque__ctor_m264171FAB7A76BF84F6A30A1F13953C20C2B0FC8 (void);
// 0x00000007 System.Void PlayVideo::stopPlay()
extern void PlayVideo_stopPlay_m9BE59571EDE6AFDC547C6A72BFC87CC7F71EF1BD (void);
// 0x00000008 System.Void PlayVideo::.ctor()
extern void PlayVideo__ctor_m12D7A6FF63237382D2F34B45F32BCAC81191FF98 (void);
// 0x00000009 System.Void PlayVideo::.cctor()
extern void PlayVideo__cctor_m75002B1E6552518DF44B60F1AD1D5C3311CFC005 (void);
// 0x0000000A System.Void Principio::Start()
extern void Principio_Start_m9C3E0ED7AD0286DA74184754ABE135AB0EAA24FD (void);
// 0x0000000B System.Void Principio::Update()
extern void Principio_Update_m13D0FB1B8B2E7699DA330F2C01D268788420F9C7 (void);
// 0x0000000C System.Void Principio::OnStartSecuence()
extern void Principio_OnStartSecuence_mBF596F2F80333266C69EA7F38ABA36AC75DF5377 (void);
// 0x0000000D System.Void Principio::.ctor()
extern void Principio__ctor_mB4D0AFE192B15FD63F8CBE35301DEF7D9D40B4EF (void);
// 0x0000000E System.Void SnapChange::Cambio()
extern void SnapChange_Cambio_mA55152BF0BDE876B55888AEC013D73330A70F033 (void);
// 0x0000000F System.Void SnapChange::.ctor()
extern void SnapChange__ctor_m9E15431F272A0917240072ABE32DDDCEA934C0E0 (void);
static Il2CppMethodPointer s_methodPointers[15] = 
{
	AutoPlay_Start_mB70D4770E116E6CF9F59384ACADD89A37DC62D20,
	AutoPlay_Update_mDFBBCA2D848E04E091E09E130B0443F0FC9B4428,
	AutoPlay__ctor_m0048F0EBF8281CE23AB62A575A8C7D2F87A3F381,
	Choque_Start_m47A0EBF9644517E33267B617CC7244C1B3CAA9C4,
	Choque_OnCollisionEnter_m49855E353909337CB23C00B96134ED2CC12ED65A,
	Choque__ctor_m264171FAB7A76BF84F6A30A1F13953C20C2B0FC8,
	PlayVideo_stopPlay_m9BE59571EDE6AFDC547C6A72BFC87CC7F71EF1BD,
	PlayVideo__ctor_m12D7A6FF63237382D2F34B45F32BCAC81191FF98,
	PlayVideo__cctor_m75002B1E6552518DF44B60F1AD1D5C3311CFC005,
	Principio_Start_m9C3E0ED7AD0286DA74184754ABE135AB0EAA24FD,
	Principio_Update_m13D0FB1B8B2E7699DA330F2C01D268788420F9C7,
	Principio_OnStartSecuence_mBF596F2F80333266C69EA7F38ABA36AC75DF5377,
	Principio__ctor_mB4D0AFE192B15FD63F8CBE35301DEF7D9D40B4EF,
	SnapChange_Cambio_mA55152BF0BDE876B55888AEC013D73330A70F033,
	SnapChange__ctor_m9E15431F272A0917240072ABE32DDDCEA934C0E0,
};
static const int32_t s_InvokerIndices[15] = 
{
	7071,
	7071,
	7071,
	7071,
	5663,
	7071,
	7071,
	7071,
	12456,
	7071,
	7071,
	7071,
	7071,
	7071,
	7071,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	15,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

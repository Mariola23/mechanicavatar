using System;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Buttons : MonoBehaviour
{
    public Scene scene;
    public DBController db;
    public GameObject panelLogin, panelCrear, panelCambiar, panelCambiar2;
    public TMP_Text textoError;
    private User _user;

    public void cambiarPanelCrear()
    {
        panelLogin.SetActive(false);
        panelCrear.SetActive(true);
        textoError.text = "";
    }

    public void cambiarPanelCambiar()
    {
        panelLogin.SetActive(false);
        panelCambiar.SetActive(true);
        textoError.text = "";
    }

    public void atras()
    {
        if (panelCrear.activeSelf)
        {
            panelCrear.SetActive(false);
            panelLogin.SetActive(true);
        }
        else if (panelCambiar.activeSelf)
        {
            panelCambiar.SetActive(false);
            panelLogin.SetActive(true);
        }
        else if (panelCambiar2.activeSelf)
        {
            panelCambiar2.SetActive(false);
            panelLogin.SetActive(true);
        }

        textoError.text = "";
    }

    public void iniciarSesion()
    {
        var nombreUsuario = panelLogin.transform.GetChild(0).gameObject.GetComponent<TMP_InputField>().text;
        var contraseña = panelLogin.transform.GetChild(1).gameObject.GetComponent<TMP_InputField>().text;

        if (nombreUsuario.IsUnityNull() || contraseña.IsUnityNull() || nombreUsuario.Length == 0 ||
            contraseña.Length == 0)
        {
            textoError.text = "El campo usuario o contraseña no puede estar vacio";

            panelLogin.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.red;
            panelLogin.transform.GetChild(1).gameObject.GetComponent<Image>().color = Color.red;

            return;
        }

        if (nombreUsuario.Contains("@"))
        {
            if (db.EncontrarUsuarioPorEmail(nombreUsuario, contraseña))
            {
                SceneManager.LoadScene("Scene");
                return;
            }

            panelLogin.transform.GetChild(1).gameObject.GetComponent<Image>().color = Color.red;

            textoError.text = "Contraseña incorrecta";
        }
        else
        {
            if (db.EncontrarUsuarioPorUsuario(nombreUsuario, contraseña))
            {
                SceneManager.LoadScene("Scene");
                return;
            }

            textoError.text = "Contraseña incorrecta";
            panelLogin.transform.GetChild(1).gameObject.GetComponent<Image>().color = Color.red;
        }
    }

    public void crearUsuario()
    {
        var contraseña = panelCrear.transform.GetChild(2).gameObject.GetComponent<TMP_InputField>().text;
        var contraseñaRepit = panelCrear.transform.GetChild(3).gameObject.GetComponent<TMP_InputField>().text;

        if (contraseña != contraseñaRepit)
        {
            textoError.text = "Las contraseñas no coinciden";

            panelCrear.transform.GetChild(2).gameObject.GetComponent<Image>().color = Color.red;
            panelCrear.transform.GetChild(3).gameObject.GetComponent<Image>().color = Color.red;
            return;
        }

        var nombreUsuario = panelCrear.transform.GetChild(0).gameObject.GetComponent<TMP_InputField>().text;
        var email = panelCrear.transform.GetChild(1).gameObject.GetComponent<TMP_InputField>().text;

        if (!email.Contains("@"))
        {
            textoError.text = "El correo no es valido";

            panelCrear.transform.GetChild(1).gameObject.GetComponent<Image>().color = Color.red;

            return;
        }

        var error = db.CrearUsuario(nombreUsuario, email, contraseña);

        if (!error.Equals("true"))
        {
            textoError.text = error;

            panelCrear.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.red;
            panelCrear.transform.GetChild(1).gameObject.GetComponent<Image>().color = Color.red;
            return;
        }

        panelCrear.transform.GetChild(0).gameObject.GetComponent<TMP_InputField>().text = "";
        panelCrear.transform.GetChild(1).gameObject.GetComponent<TMP_InputField>().text = "";
        panelCrear.transform.GetChild(2).gameObject.GetComponent<TMP_InputField>().text = "";
        panelCrear.transform.GetChild(3).gameObject.GetComponent<TMP_InputField>().text = "";

        atras();
    }

    public void comprobarCambioContraseña()
    {
        var usuario = panelCambiar.transform.GetChild(0).gameObject.GetComponent<TMP_InputField>().text;
        var email = panelCambiar.transform.GetChild(1).gameObject.GetComponent<TMP_InputField>().text;

        try
        {
            _user = db.EncontrarUsuarioPorUsuarioYEmail(usuario, email);

            panelCambiar.transform.GetChild(0).gameObject.GetComponent<TMP_InputField>().text = "";
            panelCambiar.transform.GetChild(1).gameObject.GetComponent<TMP_InputField>().text = "";

            panelCambiar.SetActive(false);
            panelCambiar2.SetActive(true);
        }
        catch (Exception)
        {
            textoError.text = "Ese usuario no existe con ese correo no existe";

            panelCambiar.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.red;
            panelCrear.transform.GetChild(1).gameObject.GetComponent<Image>().color = Color.red;
        }
    }

    public void cambiarContraseña()
    {
        var contraseñaNueva = panelCambiar2.transform.GetChild(0).gameObject.GetComponent<TMP_InputField>().text;
        var repetirContraseñaNueva = panelCambiar2.transform.GetChild(1).gameObject.GetComponent<TMP_InputField>().text;

        if (contraseñaNueva.IsUnityNull() || repetirContraseñaNueva.IsUnityNull() || contraseñaNueva.Length == 0 ||
            repetirContraseñaNueva.Length == 0)
        {
            textoError.text = "El campo contraseña nueva o repetir contraseña nueva no pueden estar vacio";
        }

        if (contraseñaNueva != repetirContraseñaNueva)
        {
            textoError.text = "Las contraseñas no coinciden";

            panelCambiar2.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.red;
            panelCambiar2.transform.GetChild(1).gameObject.GetComponent<Image>().color = Color.red;
            return;
        }

        var error = db.CambiarContraseñaUsuario(_user, contraseñaNueva);

        if (!error.Equals("true"))
        {
            textoError.text = error;

            panelCambiar2.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.red;
            panelCambiar2.transform.GetChild(1).gameObject.GetComponent<Image>().color = Color.red;
            return;
        }

        panelCambiar2.transform.GetChild(0).gameObject.GetComponent<TMP_InputField>().text = "";
        panelCambiar2.transform.GetChild(1).gameObject.GetComponent<TMP_InputField>().text = "";

        atras();
    }

    public void entrarSinContraseña()
    {
        SceneManager.LoadScene("Scene");
    }

    public void ver_Contraseña(TMP_InputField field)
    {
        if (field.contentType == TMP_InputField.ContentType.Password)
        {
            field.contentType = TMP_InputField.ContentType.Standard;
        }
        else
        {
            field.contentType = TMP_InputField.ContentType.Password;
        }
    }
}
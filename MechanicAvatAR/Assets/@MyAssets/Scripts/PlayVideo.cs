using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayVideo : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    private static Boolean stop = true;
    public GameObject playSprite;
    public GameObject stopSrpite;

    public void stopPlay()
    {
        if (stop)
        {
            videoPlayer.Play();
            stop = false;
            return;
        }
        stopSrpite.SetActive(false);
        playSprite.SetActive(true);
        videoPlayer.Stop();
        stop = true;
    }
    
}

using UnityEngine;

public class SnapChange : MonoBehaviour
{
    public GameObject ponible;
    public GameObject noPonible;
    public GameObject cambio;
    
    public void Cambio()
    {
        Transform posicion = noPonible.transform;
        ponible.SetActive(false);
        noPonible.SetActive(false);
        GameObject ob = Instantiate(cambio);
        Destroy(ponible);
        Destroy(noPonible);
        ob.transform.position = posicion.position;
    }
}

﻿using Realms;
using MongoDB.Bson;

public partial class User : IRealmObject
{
    [MapTo("_id")] [PrimaryKey] public ObjectId Id { get; set; } = ObjectId.GenerateNewId();

    public string Email { get; set; }
    public string UserName { get; set; }
    public string UserPass { get; set; }

    public User(string email, string userName, string userPass)
    {
        Email = email;
        UserName = userName;
        UserPass = userPass;
    }
}
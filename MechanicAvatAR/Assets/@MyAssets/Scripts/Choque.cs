using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Choque : MonoBehaviour
{
	public GameObject pieza1, pieza2, pieza3;
    public Animator animacion;


    void Start()
    {
        pieza3.gameObject.SetActive(false);
        animacion.enabled = false;
        animacion.gameObject.SetActive(false);
    }

    //Detect collisions between the GameObjects with Colliders attached
    void OnCollisionEnter(Collision collision){
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.CompareTag(pieza2.tag))
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            animacion.enabled = true;
            animacion.gameObject.SetActive(true);
            animacion.Play("Take 001", -1, 0f);
            
            Destroy(pieza1);
            Destroy(pieza2);
            pieza3.gameObject.SetActive(true);
        }
    }
}
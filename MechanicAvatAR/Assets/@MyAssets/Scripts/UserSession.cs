﻿using System;
using Realms;
using MongoDB.Bson;

public partial class UserSession : IRealmObject
{
    [MapTo("_id")] [PrimaryKey] public ObjectId Id { get; set; } = ObjectId.GenerateNewId();

    public DateTimeOffset LastLoginDate { get; set; }

    public string SessionData { get; set; }

    public User User { get; set; }
}
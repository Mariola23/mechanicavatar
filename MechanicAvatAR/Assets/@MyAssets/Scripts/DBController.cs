using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Realms;
using Realms.Sync;
using UnityEngine;
using UnityEngine.Serialization;

public class DBController : MonoBehaviour
{
    static public DBController instance;

    private Realm _realm;
    private App _realmApp;
    private Realms.Sync.User _user;

    [FormerlySerializedAs("_realmAppId")] [SerializeField]
    private string realmAppId = "mechanicavatar-bztsc";

    private async void Awake()
    {
        DontDestroyOnLoad(this);
        instance = this;
        if (_realm == null)
        {
            try
            {
                _realmApp = App.Create(new AppConfiguration(realmAppId));
                if (_user == null)
                {
                    _user = await _realmApp.LogInAsync(Credentials.Anonymous());
                    _realm = await Realm.GetInstanceAsync(new FlexibleSyncConfiguration(_user));
                }
                else
                {
                    _user = _realmApp.CurrentUser;
                    if (_user != null) _realm = await Realm.GetInstanceAsync(new FlexibleSyncConfiguration(_user));
                }
            }
            catch (Exception e)
            {
                _realm = Realm.GetInstance();
                Debug.LogError(e);
            }

            _realm.Subscriptions.Update(() => { _realm.Subscriptions.Add(_realm.All<User>()); });
            await _realm.Subscriptions.WaitForSynchronizationAsync();
        }
    }

    private void OnDisable()
    {
        //_realm.Subscriptions.Update(() => { _realm.Subscriptions.Remove(_realm.All<User>()); });
        _realm?.Dispose();
    }

    public bool EncontrarUsuarioPorUsuario(string usuario, string pass)
    {
        var user = _realm.All<User>().Where(u => u.UserName == usuario).Single();
        return user.UserPass == pass;
    }

    public User EncontrarUsuarioPorUsuarioYEmail(string usuario, string email)
    {
        var user = _realm.All<User>().Where(u => u.UserName == usuario && u.Email == email).Single();
        return user;
    }

    public bool EncontrarUsuarioPorEmail(string email, string pass)
    {
        var user = _realm.All<User>().Where(u => u.Email == email).Single();
        return user.UserPass == pass;
    }

    public String CrearUsuario(string usuario, string email, string pass)
    {
        try
        {
            _realm.All<User>().Where(u => u.UserName.Equals(usuario)).Single();
            return "Ya existe un usuario con ese nombre de usuario";
        }
        catch (Exception)
        {
            try
            {
                _realm.All<User>().Where(u => u.Email.Equals(email)).Single();
                return "Ya existe un usuario con ese correo";
            }
            catch (Exception)
            {
                var user = new User(email, usuario, pass);
                _realm.WriteAsync(() => { _realm.Add(user); });
                return "true";
            }
        }
    }

    public string CambiarContraseñaUsuario(User usuario, String nuevaPass)
    {
        if (usuario.UserPass == nuevaPass)
        {
            return "No puedes escribir la misma contraseña que ya tienes";
        }

        _realm.WriteAsync(() =>
        {
            usuario.UserPass = nuevaPass;
            _realm.Add(usuario, true);
        });

        return "true";
    }
}
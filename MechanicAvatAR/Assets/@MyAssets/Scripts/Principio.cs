using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Principio : MonoBehaviour
{
    public Animator animacion1, animacion2, animacion3, animacion4, animacion5;
    public VideoPlayer video1, video2, video3, video4, video5;
    public TextMeshPro paso, texto;
    public Button comenzar, siguiente;

    // Start is called before the first frame update
    void Start()
    {
        texto.gameObject.SetActive(false);
        paso.gameObject.SetActive(false);
        siguiente.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnStartSecuence()
    {
        texto.gameObject.SetActive(true);
        paso.gameObject.SetActive(true);
        siguiente.gameObject.SetActive(true);
        comenzar.gameObject.SetActive(false);
        video1.Play();
        paso.text = "Paso 1";
    }

    public void OnNextMove()
    {
        if (animacion1.isActiveAndEnabled)
        {
            video1.Stop();
            animacion1.gameObject.SetActive(false);
            paso.text = "Paso 2";
            video2.Play();
        }
        if (animacion2.isActiveAndEnabled)
        {
            video2.Stop();
            animacion2.gameObject.SetActive(false);
            paso.text = "Paso 3";
            video3.Play();
        }
        if (animacion3.isActiveAndEnabled)
        {
            video3.Stop();
            animacion3.gameObject.SetActive(false);
            paso.text = "Paso 4";
            video4.Play();
        }
        if (animacion4.isActiveAndEnabled)
        {
            video4.Stop();
            animacion4.gameObject.SetActive(false);
            paso.text = "Paso 5";
            video5.Play();
        }
        if (animacion5.isActiveAndEnabled)
        {
            video5.Stop();
            animacion5.gameObject.SetActive(false);
            paso.text = "FIN";
        }
    }
}
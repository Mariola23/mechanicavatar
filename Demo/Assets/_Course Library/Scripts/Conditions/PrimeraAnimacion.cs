using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrimeraAnimacion : MonoBehaviour
{
    public GameObject primero;
    public Animator animacion;


    void Start()
    {
        animacion.enabled = false;
        animacion.gameObject.SetActive(false);
    }

    //Detect collisions between the GameObjects with Colliders attached
    void OnCollisionEnter(Collision collision)
    {

        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject == primero)
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            Destroy(this.gameObject);
            Destroy(primero);
            animacion.gameObject.SetActive(true);
            animacion.enabled = true;
            animacion.Play("Base Layer.Take 001", 0, 0);

        }
    }
}
